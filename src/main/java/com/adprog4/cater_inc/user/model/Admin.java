package com.adprog4.cater_inc.user.model;

import com.adprog4.cater_inc.inbox.model.Notifier;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Getter
@Setter
@Entity(name = "Admin")
@DiscriminatorValue("Admin")
public class Admin extends UserC {

    @OneToOne
    private Notifier notifier;

    public Admin(String uuid, String firstName, String lastName, Notifier notifier) {
        super(uuid, firstName, lastName);
        this.notifier = notifier;
    }
}
