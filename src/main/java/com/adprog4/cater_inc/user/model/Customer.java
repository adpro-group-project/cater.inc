package com.adprog4.cater_inc.user.model;

import com.adprog4.cater_inc.inbox.model.Inbox;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity(name = "Customer")
@DiscriminatorValue("Customer")
public class Customer extends UserC{

    @Column(name = "address")
    private String address;

    @Column(name = "birthdate")
    private LocalDate birthDate;

    @OneToOne
    private Inbox inbox;

    @OneToMany
    private List<Pemesanan> listPemesanan;

    public Customer(String uuid, String firstName, String lastName, String address, LocalDate birthDate) {
        super(uuid, firstName, lastName);
        this.address = address;
        this.birthDate = birthDate;
        this.inbox = new Inbox();
    }



}
