package com.adprog4.cater_inc.user.repository;

import com.adprog4.cater_inc.menu.model.Topping;
import com.adprog4.cater_inc.user.model.UserC;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<UserC, Long> {
    @Query(value = "SELECT * FROM product WHERE product_type LIKE 'Beverage' " +
            "AND user_type LIKE 'Customer'", nativeQuery = true)
    List<Topping> findAllCustomerUser();

    @Query(value = "SELECT * FROM product WHERE product_type LIKE 'Beverage' " +
            "OR user_type LIKE 'Admin'", nativeQuery = true)
    List<Topping> findAllAdminUser();
}
