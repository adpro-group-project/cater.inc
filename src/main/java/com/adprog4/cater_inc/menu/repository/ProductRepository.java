package com.adprog4.cater_inc.menu.repository;

import com.adprog4.cater_inc.menu.model.Product;
import com.adprog4.cater_inc.menu.model.Topping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "SELECT * FROM product WHERE product_type LIKE 'Beverage' " +
            "OR product_type LIKE 'Food'", nativeQuery = true)
    List<Topping> findAllTopping();

    @Query(value = "SELECT * FROM product WHERE product_type LIKE 'Packet' " +
            "OR product_type LIKE 'CustomPacket'", nativeQuery = true)
    List<Product> findAllProductNoTopping();

    @Query(value = "SELECT * FROM product WHERE (product_type LIKE 'Beverage' " +
            "OR product_type LIKE 'Food') AND name = ?1", nativeQuery = true)
    Optional<Topping>findToppingByName(String name);

    @Query(value = "SELECT * FROM product WHERE (product_type LIKE 'Packet' " +
            "OR product_type LIKE 'CustomPacket') AND name = ?1", nativeQuery = true)
    Optional<Product>findProductNoToppingByName(String name);




}
