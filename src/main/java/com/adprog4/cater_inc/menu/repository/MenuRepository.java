package com.adprog4.cater_inc.menu.repository;


import com.adprog4.cater_inc.menu.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {

}
