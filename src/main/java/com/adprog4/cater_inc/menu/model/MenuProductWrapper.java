package com.adprog4.cater_inc.menu.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/*
For JSON Parsing
Menu and Product Pair ("Without Topping")
 */
@Getter
@Setter
public class MenuProductWrapper {
    Menu menu;
    Product product;

    public MenuProductWrapper(Menu menu, Product product) {
        this.menu = menu;
        this.product = product;
    }

}
