package com.adprog4.cater_inc.menu.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@JsonTypeName("Beverage")
@Entity
@DiscriminatorValue("Beverage")
public class Beverage extends Topping{

    public Beverage(){
        super();
    }

    public Beverage(long id, String name, double price, long quantity) {
        super(id, name, price, quantity);

    }

    public Beverage(long id, String name, BigDecimal price, long quantity) {
        super(id, name, price, quantity);

    }

    public Beverage(Beverage beverage){
        super(beverage);
    }

    @Override
    public BigDecimal getRealPrice() {
        if(this.price == null) return null;
        if (this.product == null){
            return this.price
                    .multiply(BigDecimal.valueOf(this.quantity));
        }
        return this.price
                .multiply(BigDecimal.valueOf(this.quantity))
                .add(this.product.getRealPrice());
    }


    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Beverage){
            return super.equals(obj);
        }
        return false;
    }


}
