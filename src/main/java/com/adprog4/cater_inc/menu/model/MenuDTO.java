package com.adprog4.cater_inc.menu.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MenuDTO {
    private long id;

    private BigDecimal price;

    private Product product;

    private Product base;

    private Map<String, Integer> content;

    public MenuDTO(Menu menu){
        this.id = menu.getId();
        this.price = menu.getPrice();
        this.base = menu.getBase();
        this.product = menu.getProduct();
        this.content = menu.getContent();
    }

    public Menu convertToMenu(){
        Menu menu = new Menu(id, base);
        menu.setProduct(product);
        menu.setContent(content);
        return menu;
    }
}
