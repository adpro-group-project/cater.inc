package com.adprog4.cater_inc.menu.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Map;

@JsonTypeName("Packet")
@Entity(name = "Packet")
@DiscriminatorValue("Packet")
public class Packet extends Product {

    @ElementCollection
    @MapKeyColumn(name="name")
    @Column(name="quantity")
    @CollectionTable(name="product_content", joinColumns=@JoinColumn(name="packet_id"))
    private Map<String, Integer> content;

    @Column(name = "stock")
    private long stock;

    public Packet(){
        super();
    }

    public Packet(long id, String name, BigDecimal price, Map<String, Integer> content,long stock) {
        super(id, name, price);
        this.content = content;
        this.stock = stock;
    }

    public Packet(long id, String name, double price, Map<String, Integer> content,long stock) {
        super(id, name, price);
        this.content = content;
        this.stock = stock;
    }



    public Packet(Packet packet){
        super(packet);
        this.content = packet.getContent();
    }

    public Map<String, Integer> getContent() {
        return content;
    }

    public void setContent(Map<String, Integer> content) {
        this.content = content;
    }

    @Override
    public BigDecimal getRealPrice() {
        return this.price;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Packet){
            Packet newObj = (Packet) obj;
            return super.equals(obj) && this.content.equals(newObj.getContent());
        }
        return false;
    }


    public long getStock() { return this.stock; }


    public void setStock(long numStock) {
        this.stock = numStock;
    }
}
