package com.adprog4.cater_inc.menu.model;



import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/*
Wrapper For Decorator Pattern
 */
@Entity
@Table(name = "menu")
public class Menu{

    @Id
    private long id;

    @Column(name = "price")
    private BigDecimal price;

    @Transient
    private Product product;

    @ManyToOne
    private Product base;

    @ElementCollection
    @MapKeyColumn(name="name")
    @Column(name="quantity")
    @CollectionTable(name="menu_content", joinColumns=@JoinColumn(name="menu_id"))
    private Map<String, Integer> content;

    public Menu(){
        super();
    }

    public Menu(long id, Product product){
        this.id = id;
        this.base = product;
        this.product = product;
        content = new HashMap<>();
        this.getPrice();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setBase(Product product){
        if(!(product instanceof Topping)){
            if (!(this.product instanceof Topping)){
                this.product = product;
                this.base = product;
            }
            else{
                Topping afterBase = (Topping) searchAfterProduct(base);
                this.base = product;
                afterBase.setProduct(product);
            }
            updatePrice();
        }
    }

    public Product getBase() {

        return base;
    }

    public Product getProduct() {

        return product;
    }

    public void setProduct(Product product) {

        this.product = product;
    }

    public Map<String, Integer> getContent() {
        return content;
    }

    public void setContent(Map<String, Integer> content) {

        this.content = content;
    }

    public BigDecimal getPrice(){
        if (this.price == null){
            updatePrice();
        }
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void addTopping(Topping topping){
        if (checkToppingExist(topping)){
            Topping toppingAdd = null;
            if(this.product.equals(topping)){
                toppingAdd = (Topping) this.product;
            }
            else {
             Topping afterToppingAdd = (Topping) searchAfterProduct(topping);
             toppingAdd = (Topping) afterToppingAdd.getProduct();
            }
            toppingAdd.setQuantity(topping.getQuantity() +1);

        }
        else {
            topping.setQuantity(1);
            topping.setProduct(this.product);
            this.product = topping;
            content.put(topping.getName(), 1);}

        updatePrice();
    }

    public void removeTopping(Topping topping){
        if (checkToppingExist(topping)){
            if (this.product.equals(topping)){
                Topping toppingDelete = (Topping) this.product;
                this.product = removeToppingAndHandleZeroQuantity(toppingDelete);
            }
            else {
                Topping afterToppingDelete = (Topping) searchAfterProduct(topping);
                Topping toppingDelete = (Topping) afterToppingDelete.getProduct();
                afterToppingDelete.setProduct(
                        removeToppingAndHandleZeroQuantity(toppingDelete));
            }
            removeContent(topping.getName());
            updatePrice();
        }
    }




    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Menu){
            Menu menu = (Menu) obj;
            return menu.content.equals(this.content) &&
                    menu.base.equals(this.base) &&
                    menu.price.equals(this.price);
        }
        return false;
    }

    private void removeContent(String name){
        content.put(name, content.get(name) - 1);
        if(content.get(name) == 0){
            content.remove(name);}
    }

    private Product searchAfterProduct(Product product){
        Product nowTopping = this.product;
        while (!((Topping)nowTopping).getProduct().equals(product)){
            nowTopping = ((Topping)nowTopping).getProduct();
        }
        return nowTopping;
    }

    private Product removeToppingAndHandleZeroQuantity(Topping topping){
       topping.setQuantity(topping.getQuantity()-1);
       if(topping.getQuantity() == 0){
           return topping.getProduct();
       }
       return topping;

    }

    private void updatePrice(){
        this.price = this.product.getRealPrice();
    }

    private boolean checkToppingExist(Topping topping){
        return this.content.containsKey(topping.getName());
    }

}
