package com.adprog4.cater_inc.menu.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;
import java.math.BigDecimal;


@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Food.class, name = "Food"),
        @JsonSubTypes.Type(value = Beverage.class, name = "Beverage")
})
@Entity(name = "Topping")
@DiscriminatorValue("Topping")
public abstract class Topping extends Product {
    @Transient
    protected Product product;

    @Column(name = "quantity")
    protected long quantity;

    public Topping(){
        super();
    }

    public Topping(long id, String name, BigDecimal price, long quantity) {
        super(id, name, price);
        this.quantity = quantity;
    }

    public Topping(long id, String name, double price, long quantity) {
        super(id, name, price);
        this.quantity = quantity;
    }

    public Topping(Topping topping){
        super(topping);
        this.quantity = 1;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Override
    public abstract BigDecimal getRealPrice();


    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Topping){
            Topping newObj = (Topping) obj;
            return this.name.equals(newObj.getName()) &&
                    this.price.compareTo(newObj.getPrice()) == 0;
        }
        return false;
    }


}
