package com.adprog4.cater_inc.menu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class ProductDTO {
    private String type;
    private long id;
    private String name;
    private BigDecimal price;
    private Map<String, Integer> content;
    private long stock;


    public Product convertToProduct(){
        Product product = null;
        if (type.equalsIgnoreCase("Packet")){
            product = new Packet(this.id, this.name,
                    this.price, this.content,
                    this.stock);
        }
        else if(type.equalsIgnoreCase("CustomPacket")){
            product = new CustomPacket(this.id);
        }
        return product;
    }


}
