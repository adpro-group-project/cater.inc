package com.adprog4.cater_inc.menu.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;
import java.math.BigDecimal;


@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Packet.class, name = "Packet"),
        @JsonSubTypes.Type(value = CustomPacket.class, name = "CustomPacket"),
        @JsonSubTypes.Type(value = Topping.class, name = "Topping")
})
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "product_type")
public abstract class Product{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected long id;

    @Column(name = "name", unique = true)
    protected String name;

    @Column(name = "price")
    protected BigDecimal price;


    public Product(){
        super();
    }

    public Product(long id, String name, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Product(long id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = BigDecimal.valueOf(price);
    }

    public Product(Product product){
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }


    public BigDecimal getPrice(){
        return this.price;
    }

    public abstract BigDecimal getRealPrice();

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Product){
            Product newObj = (Product) obj;
            return this.name.equals(newObj.getName()) &&
                    this.price.compareTo(newObj.getPrice()) == 0;
        }
        return false;
    }
}
