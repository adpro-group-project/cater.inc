package com.adprog4.cater_inc.menu.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/*
For JSON Parsing
Menu and Topping Pair
 */
@Getter
@Setter
public class MenuToppingWrapper {
    Menu menu;
    Topping topping;

    public MenuToppingWrapper(Menu menu, Topping topping){
        this.menu = menu;
        this.topping = topping;
    }
}
