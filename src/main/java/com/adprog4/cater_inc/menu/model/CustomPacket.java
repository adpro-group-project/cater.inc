package com.adprog4.cater_inc.menu.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@JsonTypeName("CustomPacket")
@Entity(name = "CustomPacket")
@DiscriminatorValue("CustomPacket")
public class CustomPacket extends Product{

    public CustomPacket(long id) {
        super(id, "Custom",BigDecimal.ZERO);
    }
    public CustomPacket(){
        super();
    }



    @Override
    public BigDecimal getRealPrice(){return this.price;}


}
