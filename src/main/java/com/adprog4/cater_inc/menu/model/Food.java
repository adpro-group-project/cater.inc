package com.adprog4.cater_inc.menu.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@JsonTypeName("Food")
@Entity
@DiscriminatorValue("Food")
public class Food extends Topping {

    public Food(){
        super();
    }

    public Food(long id, String name, double price, long quantity) {
        super(id, name, price, quantity );
    }

    public Food(long id, String name, BigDecimal price, long quantity) {
        super(id, name, price, quantity);
    }

    public Food(Food food){
        super(food);
    }

    @Override
    public BigDecimal getRealPrice() {
        if (this.price == null) return null;
        if (this.product == null){
            return this.price.multiply(BigDecimal.valueOf(this.quantity));
        }
        return this.price.multiply(BigDecimal.valueOf(this.quantity))
                .add(this.product.getRealPrice());
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Food){
            return super.equals(obj);
        }
        return false;
    }

}
