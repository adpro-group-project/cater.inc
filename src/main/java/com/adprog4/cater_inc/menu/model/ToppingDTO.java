package com.adprog4.cater_inc.menu.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;



@Getter
@Setter
@AllArgsConstructor
public class ToppingDTO {
    private String type;
    private long id;
    private String name;
    private BigDecimal price;
    private long quantity;


    public Topping convertToTopping(){
        Topping topping = null;
        if (this.type.equalsIgnoreCase("Food")){
            topping = new Food(id, name, price, quantity);
        }
        else if(this.type.equalsIgnoreCase("Beverage")){
            topping = new Beverage(id, name, price, quantity);
        }
        return topping;
    }
}
