package com.adprog4.cater_inc.menu.service;

import com.adprog4.cater_inc.menu.model.*;
import com.adprog4.cater_inc.menu.repository.MenuRepository;
import com.adprog4.cater_inc.menu.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.io.InvalidObjectException;
import java.math.BigDecimal;
import java.util.*;

@Service
public class MenuServiceimpl implements MenuService {

    private final MenuRepository menuRepository;
    private final ProductRepository productRepository;
    private final Random idGenerator;

    public MenuServiceimpl(MenuRepository menuRepository, ProductRepository productRepository){
        this.menuRepository = menuRepository;
        this.productRepository = productRepository;
        this.idGenerator = new Random();
    }

    @Override
    public List<Product> getAllProducts() {
        return this.productRepository.findAllProductNoTopping();
    }

    @Override
    public Optional<Product> findProductByName(String name) {
        return this.productRepository.findProductNoToppingByName(name);
    }

    @Override
    public List<Topping> getAllToppings() {

        return this.productRepository.findAllTopping();
    }

    @Override
    public Optional<Topping> findToppingByName(String name)
    {

        return this.productRepository.findToppingByName(name);
    }

    @Override
    public List<Menu> getAllMenu() {
        return menuRepository.findAll();
    }

    @Override
    public void addProduct(ProductDTO productDTO){
        Product product = productDTO.convertToProduct();
        if(checkProductAndToppingIdExist(product.getId())){
            long idProd = generateId();
            idProd = Math.abs(idProd);
            while (checkMenuIdExist(idProd)){
                idProd = generateId();
            }
            product.setId(idProd);
        }
        productRepository.save(product);

    }

    @Override
    public void addTopping(ToppingDTO toppingDTO) {
        Topping topping = toppingDTO.convertToTopping();
        if(checkProductAndToppingIdExist(topping.getId())){
            long idTop = generateId();
            while (checkMenuIdExist(idTop)){
                idTop = generateId();
            }
            topping.setId(idTop);
        }
        productRepository.save(topping);
    }

    @Override
    public Menu addMenu(MenuDTO menuDTO) throws InvalidObjectException {
        Menu menu = menuDTO.convertToMenu();
        if(!checkMenuComponentIsValid(menu)){
            throw new InvalidObjectException("Menu Component Corrupted / Tinkered");
        }
        if(checkMenuIdExist(menu.getId())){
            long idMenu = generateId();
            while (checkMenuIdExist(idMenu)){
                idMenu = generateId();
            }
            menu.setId(idMenu);
        }
        menuRepository.save(menu);
        return menu;
    }


    @Override
    public Menu menuSetBase(MenuProductWrapper menuProd) {
        Menu menu = menuProd.getMenu();
        menu.setBase(menuProd.getProduct());
        return menu;
    }

    @Override
    public Menu menuAddTopping(MenuToppingWrapper menuTop) {
        Menu menu = menuTop.getMenu();
        menu.addTopping(menuTop.getTopping());
        return menu;
    }

    @Override
    public Menu menuRemoveTopping(MenuToppingWrapper menuTop){
        Menu menu = menuTop.getMenu();
        menu.removeTopping(menuTop.getTopping());
        return menu;
    }



    @Override
    public BigDecimal checkMenuPrice(MenuDTO menuDTO) {
        Menu menu = menuDTO.convertToMenu();
        menu.setPrice(null);
        return menu.getPrice();
    }

    private boolean checkProductAndToppingIdExist(long id) {
        Optional<Product> product = productRepository.findById(id);
        return product.isPresent();
    }

    private boolean checkMenuIdExist(long id) {
        Optional<Menu> menu = menuRepository.findById(id);
        return menu.isPresent();
    }

    private boolean checkMenuComponentIsValid(Menu menu){
        try{
            return checkChainProductValid(menu.getProduct());
        }
        catch (NullPointerException e){
            return false;
        }

    }


    private boolean checkChainProductValid(Product product){
        if(!(product instanceof Topping)){
            Optional<Product> productGet = productRepository
                    .findProductNoToppingByName(product.getName());
            return checkOptionalProductEqualsProduct(productGet, product);
        }
        else {
            Optional<Topping> toppingGet = productRepository
                    .findToppingByName(product.getName());
            return checkOptionalToppingEqualsProduct(toppingGet, product) &&
                    checkChainProductValid(((Topping)product).getProduct());
        }

    }

    private boolean checkOptionalProductEqualsProduct(Optional<Product> optionalProduct, Product product){
        if(optionalProduct.isPresent()){
            Product productTest = optionalProduct.get();
            return product.equals(productTest);
        }
        return false;
    }
    private boolean checkOptionalToppingEqualsProduct(Optional<Topping> optionalTopping, Product product){
        if(optionalTopping.isPresent()){
            Topping toppingTest = optionalTopping.get();
            return product.equals(toppingTest);
        }
        return false;
    }

    private long generateId(){
        long num = idGenerator.nextInt();
        return Math.abs(num);
    }
}