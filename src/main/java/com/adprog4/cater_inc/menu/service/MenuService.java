package com.adprog4.cater_inc.menu.service;

import com.adprog4.cater_inc.menu.model.*;

import java.io.InvalidObjectException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface MenuService {



    //Product
    List<Product> getAllProducts();
    Optional<Product> findProductByName(String name);

    //Product
    List<Topping> getAllToppings();
    Optional<Topping> findToppingByName(String name);

    void addProduct(ProductDTO productDTO);
    void addTopping(ToppingDTO toppingDTO);
    Menu addMenu(MenuDTO menuDTO) throws InvalidObjectException;

    //Menu
    List<Menu> getAllMenu();
    Menu menuSetBase(MenuProductWrapper menuProd);
    Menu menuAddTopping(MenuToppingWrapper menuTop);
    Menu menuRemoveTopping(MenuToppingWrapper menuTop);
    BigDecimal checkMenuPrice(MenuDTO menuDTO);

}
