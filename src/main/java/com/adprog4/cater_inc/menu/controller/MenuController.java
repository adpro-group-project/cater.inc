package com.adprog4.cater_inc.menu.controller;

import com.adprog4.cater_inc.menu.model.*;
import com.adprog4.cater_inc.menu.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.io.InvalidObjectException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;



@RestController
@RequestMapping(path = "/menu")
public class MenuController {

    @Autowired
    MenuService menuService;

    @GetMapping("/prod")
    public ResponseEntity<List<Product>> findAllProducts() {
        return new ResponseEntity<>(menuService.getAllProducts(), HttpStatus.OK);
    }

    @GetMapping("/top")
    public ResponseEntity<List<Topping>> findAllTopping() {
        return new ResponseEntity<>(menuService.getAllToppings(), HttpStatus.OK);
    }

    @GetMapping("/prod/{name}")
    public ResponseEntity<Product> findProductByName(@PathVariable String name) {
        Optional<Product> productOptional = menuService.findProductByName(name);
        if(productOptional.isPresent()){
            return new ResponseEntity<>(productOptional.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/top/{name}")
    public ResponseEntity<Topping> findToppingByName(@PathVariable String name) {
        Optional<Topping> toppingOptional = menuService.findToppingByName(name);
        if(toppingOptional.isPresent()){
            return new ResponseEntity<>(toppingOptional.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/price")
    public ResponseEntity<BigDecimal> checkMenuPrice(@RequestBody MenuDTO menu){
        return new ResponseEntity<>(menuService.checkMenuPrice(menu), HttpStatus.OK);
    }

    @PostMapping("/add/top")
    public ResponseEntity addTopping(@RequestBody ToppingDTO topping){
        try {
            menuService.addTopping(topping);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/add/prod")
    public ResponseEntity addProduct(@RequestBody ProductDTO product){
        try{
            menuService.addProduct(product);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }

    //Menu
    @GetMapping()
    public ResponseEntity<List<Menu>> findAllMenu(){
        return new ResponseEntity<>(menuService.getAllMenu(), HttpStatus.OK);
    }

    @PostMapping("/add/menu")
    public ResponseEntity<Menu> addMenu(@RequestBody MenuDTO menu) throws InvalidObjectException {
        try {
            Menu menu1 = menuService.addMenu(menu);
            return new ResponseEntity<>(menu1,HttpStatus.OK);
        }
        catch (InvalidObjectException e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/menu/base")
    public ResponseEntity<Menu> menuSetBase(@RequestBody MenuProductWrapper menuProd){
        Menu menu = menuService.menuSetBase(menuProd);
        return new ResponseEntity<>(menu, HttpStatus.OK);
    }

    @PostMapping("/menu/add/top")
    public ResponseEntity<Menu> menuAddTopping(@RequestBody MenuToppingWrapper menuTop){
        Menu menu = menuService.menuAddTopping(menuTop);
        return new ResponseEntity<>(menu, HttpStatus.OK);
    }

    @PostMapping("/menu/rm/top")
    public ResponseEntity<Menu> menuRemoveTopping(@RequestBody MenuToppingWrapper menuTop){
        Menu menu = menuService.menuRemoveTopping(menuTop);
        return new ResponseEntity<>(menu, HttpStatus.OK);
    }

}

