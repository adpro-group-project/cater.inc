package com.adprog4.cater_inc.statuspemesanan.repository;

import com.adprog4.cater_inc.statuspemesanan.model.StatusPemesanan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusPemesananRepository extends JpaRepository<StatusPemesanan, Long> {

}
