package com.adprog4.cater_inc.statuspemesanan.controller;

import com.adprog4.cater_inc.statuspemesanan.model.StatusPemesanan;
import com.adprog4.cater_inc.statuspemesanan.service.StatusPemesananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/status")
public class StatusPemesananController {

    @Autowired
    private StatusPemesananService statusPemesananService;
    public StatusPemesananController(StatusPemesananService statusPemesananService){
        this.statusPemesananService = statusPemesananService;
    }

    @GetMapping("status/{idPemesanan}")
    public String showStatus(@PathVariable long idPemesanan, Model model){
        StatusPemesanan statusPemesanan = statusPemesananService.showStatusPemesanan(idPemesanan);
        try{
            model.addAttribute("idPemesanan", idPemesanan);
            model.addAttribute("statusPemesanan", statusPemesanan);
        }
        catch(NullPointerException e){
            model.addAttribute("notFound", true);
        }
        return "pemesanan/statusPemesanan";
    }
}

