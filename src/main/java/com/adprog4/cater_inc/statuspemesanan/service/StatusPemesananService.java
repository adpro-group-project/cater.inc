package com.adprog4.cater_inc.statuspemesanan.service;

import com.adprog4.cater_inc.statuspemesanan.model.StatusPemesanan;

public interface StatusPemesananService {
    public StatusPemesanan showStatusPemesanan(long idPemesanan);
}
