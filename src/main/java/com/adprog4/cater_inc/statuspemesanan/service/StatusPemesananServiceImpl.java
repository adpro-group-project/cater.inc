package com.adprog4.cater_inc.statuspemesanan.service;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import com.adprog4.cater_inc.pemesanan.factory.repository.PemesananRepository;
import com.adprog4.cater_inc.statuspemesanan.model.StatusPemesanan;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StatusPemesananServiceImpl implements StatusPemesananService{

    private final PemesananRepository pemesananRepository;

    public StatusPemesananServiceImpl(PemesananRepository pemesananRepository) {
        this.pemesananRepository = pemesananRepository;
    }

    @Override
    public StatusPemesanan showStatusPemesanan(long idPemesanan) {
        Optional<Pemesanan> pemesanan = pemesananRepository.findById(idPemesanan);
        if(pemesanan.isPresent()){
            return pemesanan.get().getStatus();
        }
        return null;
    }

}