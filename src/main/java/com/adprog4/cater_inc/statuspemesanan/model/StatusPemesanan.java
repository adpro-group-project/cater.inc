package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "status_type")
public abstract class StatusPemesanan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    @OneToOne
    public Pemesanan pemesanan;

    public StatusPemesanan(Pemesanan pemesanan){
        this.pemesanan = pemesanan;
    }

    public abstract String buat();
    public abstract String konfirmasi(boolean kondisi);
    public abstract String proses();
    public abstract String kirim();
    public abstract String selesai();
    public abstract String status();
    public abstract String gagal();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Pemesanan getPemesanan() {
        return pemesanan;
    }

    public void setPemesanan(Pemesanan pemesanan) {
        this.pemesanan = pemesanan;
    }
}
