package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "DiProses")
@DiscriminatorValue("DiProses")
public class PemesananDiProses extends StatusPemesanan{

    public PemesananDiProses(Pemesanan pemesanan){
        super(pemesanan);
    }

    @Override
    public String buat() {
        return "[REJECT]: Pemesanan tidak dapat dibuat lagi";
    }

    @Override
    public String konfirmasi(boolean kondisi) {
        return "[REJECT]: Pemesanan sudah dikonfirmasi";
    }

    @Override
    public String proses() {
        return "[REJECT]: Pemesanan masih dalam tahap proses";
    }

    @Override
    public String kirim() {
        StatusPemesanan statusBaru = new PemesananDiKirim(pemesanan);
        statusBaru.setId(this.id);
        pemesanan.setStatus(statusBaru);
        return "[OKE]: Pemesanan dikirim ke alamat pembeli";
    }

    @Override
    public String selesai() {
        return "[REJECT] :Pemesanan masih dalam tahap proses";
    }

    @Override
    public String status() {
        return "Pemesanan Di Proses";
    }

    @Override
    public String gagal() {
        return "[REJECT]: Pemesanan tidak dapat gagal pada status ini";
    }

}
