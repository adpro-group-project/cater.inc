package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "Kosong")
@DiscriminatorValue("Kosong")
public class PemesananKosong extends StatusPemesanan{
    String rejectStatus = "[REJECT]: Pesanan belum dibuat";

    public PemesananKosong(Pemesanan pemesanan) {
        super(pemesanan);
    }

    @Override
    public String buat() {
        StatusPemesanan statusBaru = new PemesananMenungguKonfirmasi(pemesanan);
        statusBaru.setId(this.id);
        pemesanan.setStatus(statusBaru);
        return "[OKE]: Pesanan Dibuat";
    }

    @Override
    public String konfirmasi(boolean kondisi) {
        return rejectStatus;
    }

    @Override
    public String proses() {
        return rejectStatus;
    }

    @Override
    public String kirim() {
        return rejectStatus;
    }

    @Override
    public String selesai() {
        return rejectStatus;
    }

    @Override
    public String status() {
        return "Pemesanan Kosong";
    }

    @Override
    public String gagal() {
        return "[REJECT]: Pemesanan tidak dapat gagal pada status ini";
    }
}
