package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "DiKonfirmasi")
@DiscriminatorValue("DiKonfirmasi")
public class PemesananDiKonfirmasi extends StatusPemesanan {

    public PemesananDiKonfirmasi(Pemesanan pemesanan) {
        super(pemesanan);
    }

    @Override
    public String buat() {
        return "[REJECT]: Pemesanan tidak dapat dibuat lagi";
    }

    @Override
    public String konfirmasi(boolean kondisi) {
        return "[REJECT]: Pemesanan tidak dapat dikofirmasi lagi";
    }

    @Override
    public String proses() {
        StatusPemesanan statusBaru = new PemesananDiProses(pemesanan);
        statusBaru.setId(this.id);
        pemesanan.setStatus(statusBaru);
        return "[OKE]: Pemesanan sedang diproses";
    }

    @Override
    public String kirim() {
        return "[REJECT]: Pemesanan masih dalam tahap proses";
    }

    @Override
    public String selesai() {
        return "[REJECT] :Pemesanan masih dalam tahap proses";
    }

    @Override
    public String status() {
        return "Pemesanan Di Konfirmasi";
    }

    @Override
    public String gagal() {
        return "[REJECT]: Pemesanan tidak dapat gagal pada status ini";
    }
}