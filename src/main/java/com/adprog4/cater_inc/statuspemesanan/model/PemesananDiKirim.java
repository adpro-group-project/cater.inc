package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("DiKirim")
public class PemesananDiKirim extends StatusPemesanan{

    public PemesananDiKirim(Pemesanan pemesanan){
        super(pemesanan);
    }


    @Override
    public String buat() {
        return "[REJECT]: Pemesanan tidak dapat dibuat lagi";
    }

    @Override
    public String konfirmasi(boolean kondisi) {
        return "[REJECT]: Pemesanan tidak dapat dikofirmasi lagi";
    }

    @Override
    public String proses() {
        return "[REJECT]: Pemesanan sudah dalam tahap pengiriman";
    }

    @Override
    public String kirim() {
        return "[REJECT]: Pemesanan sudah dalam tahap pengiriman";
    }

    @Override
    public String selesai() {
        StatusPemesanan statusPemesanan = new PemesananSelesai(pemesanan);
        statusPemesanan.setId(this.id);
        pemesanan.setStatus(statusPemesanan);
        return "[OKE]: Pemesanan sudah sampai dialamat pembeli";
    }

    @Override
    public String status() {
        return "Pemesanan Di Kirim";
    }

    @Override
    public String gagal() {
        pemesanan.setStatus(new PemesananGagal(pemesanan));
        return "[OKE]: Pemesanan gagal dalam pengiriman";
    }
}
