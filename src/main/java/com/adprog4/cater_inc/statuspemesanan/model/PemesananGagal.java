package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "Gagal")
@DiscriminatorValue("Gagal")
public class PemesananGagal extends StatusPemesanan {
    String pemesananTelahGagal = "[REJECT]: Pemesanan ini telah gagal dilakukan.";
    public PemesananGagal(Pemesanan pemesanan){
        super(pemesanan);
    }


    @Override
    public String buat() {
        return pemesananTelahGagal;
    }

    @Override
    public String konfirmasi(boolean kondisi) {
        return pemesananTelahGagal;
    }

    @Override
    public String proses() {
        return pemesananTelahGagal;
    }

    @Override
    public String kirim() {
        StatusPemesanan statusBaru = new PemesananDiKirim(pemesanan);
        statusBaru.setId(this.id);
        pemesanan.setStatus(statusBaru);
        return "[OKE]: Pemesanan akan dikirim ulang.";
    }

    @Override
    public String selesai() {
        return pemesananTelahGagal;
    }

    @Override
    public String status() {
        return "Pemesanan gagal.";
    }

    @Override
    public String gagal() {
        return pemesananTelahGagal;
    }
}
