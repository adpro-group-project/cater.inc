package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "MenungguKonfirmasi")
@DiscriminatorValue("MenungguKonfirmasi")
public class PemesananMenungguKonfirmasi extends StatusPemesanan {
    String pemesananBelumDikonfirm = "[REJECT]: Pemesanan belum dikonfirmasi";
    public PemesananMenungguKonfirmasi(Pemesanan pemesanan){
        super(pemesanan);
    }

    @Override
    public String buat() {
        return "[REJECT]: Pemesanan tidak dapat dibuat lagi";
    }

    @Override
    public String konfirmasi(boolean kondisi) {
        StatusPemesanan pemesananJadi = null;
        if(kondisi == true){
            pemesananJadi = new PemesananDiProses(pemesanan);
            pemesananJadi.setId(this.id);
            pemesanan.setStatus(pemesananJadi);
            return "[OKE]: Pemesanan dikonfirmasi";
        }
        else{
            pemesananJadi = new PemesananGagal(pemesanan);
            pemesananJadi.setId(this.id);
            pemesanan.setStatus(pemesananJadi);
            return "[OKE]: Pemesanan ditolak";
        }
    }

    @Override
    public String proses() {
        return pemesananBelumDikonfirm;
    }

    @Override
    public String kirim() {
        return pemesananBelumDikonfirm;
    }

    @Override
    public String selesai() {
        return pemesananBelumDikonfirm;
    }

    @Override
    public String status() {
        return "Pemesanan Menunggu Konfirmasi";
    }

    @Override
    public String gagal() {
        return "[REJECT]: Pemesanan tidak dapat gagal pada status ini";
    }
}
