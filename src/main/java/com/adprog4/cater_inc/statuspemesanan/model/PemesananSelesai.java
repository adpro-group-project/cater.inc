package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "Selesai")
@DiscriminatorValue("Selesai")
public class PemesananSelesai extends StatusPemesanan{

    String pemesananSudahSelesai = "[REJECT]: Pemesanan sudah selesai.";

    public PemesananSelesai(Pemesanan pemesanan){
        super(pemesanan);
    }

    @Override
    public String buat() {
        return pemesananSudahSelesai;
    }

    @Override
    public String konfirmasi(boolean kondisi) {
        return pemesananSudahSelesai;
    }

    @Override
    public String proses() {
        return pemesananSudahSelesai;
    }

    @Override
    public String kirim() {
        return pemesananSudahSelesai;
    }

    @Override
    public String selesai() {
        return pemesananSudahSelesai;
    }

    @Override
    public String status() {
        return "Pemesanan Selesai";
    }

    @Override
    public String gagal() {
        return pemesananSudahSelesai;
    }
}
