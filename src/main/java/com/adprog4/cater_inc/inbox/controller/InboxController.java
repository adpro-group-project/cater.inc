package com.adprog4.cater_inc.inbox.controller;

import com.adprog4.cater_inc.inbox.model.Inbox;
import com.adprog4.cater_inc.inbox.model.Message;
import com.adprog4.cater_inc.inbox.model.Notifier;
import com.adprog4.cater_inc.inbox.service.InboxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "")
public class InboxController {

    @Autowired
    InboxService inboxService;

    @GetMapping("/notifier/{id}")
    public ResponseEntity<Notifier> findNotifierById(@PathVariable Long id) {
        Optional<Notifier> notifierOptional = inboxService.getNotifierById(id);
        Notifier notifier = notifierOptional.get();
        return new ResponseEntity<>(notifier, HttpStatus.OK);
    }

    @PostMapping("/notifier/create")
    public ResponseEntity createNotifier(@RequestBody Notifier notifier) {
        try {
            inboxService.createNotifier(notifier);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/notifier/update")
    public ResponseEntity updateNotifier(@RequestBody Notifier notifier) {
        try {
            inboxService.createNotifier(notifier);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/notifier/delete")
    public ResponseEntity deleteNotifier(@PathVariable Long id) {
        inboxService.deleteNotifier(id);
        return new ResponseEntity(HttpStatus.OK);
    }

//    @GetMapping("/notifier/addInbox")

    @GetMapping("/inbox/findAll")
    public ResponseEntity<List<Inbox>> getAllInboxes() {
        List<Inbox> inboxList = inboxService.getAllInboxes();
        return new ResponseEntity<>(inboxList, HttpStatus.OK);
    }

    @GetMapping("/inbox/{id}")
    public ResponseEntity<Inbox> findInboxById(@PathVariable Long id) {
        Optional<Inbox> inboxOptional = inboxService.getInboxById(id);
        Inbox inbox = inboxOptional.get();
        return new ResponseEntity<>(inbox, HttpStatus.OK);
    }

    @GetMapping("/inbox/findAllMessages/{id}")
    public ResponseEntity<List<Message>> findAllMessageInInbox(@PathVariable Long id) {
        List<Message> messages = inboxService.getAllMessagesInInbox(id);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @PostMapping("/inbox/create")
    public ResponseEntity createInbox(@RequestBody Inbox inbox) {
        try {
            inboxService.createInbox(inbox);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/inbox/update")
    public ResponseEntity updateInbox(@RequestBody Inbox inbox) {
        try {
            inboxService.createInbox(inbox);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/inbox/delete")
    public ResponseEntity deleteInbox(@PathVariable Long id) {
        inboxService.deleteInbox(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/message/findAll")
    public ResponseEntity<List<Message>> findAllMessages() {
        List<Message> inboxList = inboxService.getAllMessages();
        return new ResponseEntity<>(inboxList, HttpStatus.OK);
    }

    @GetMapping("/message/{id}")
    public ResponseEntity<Message> findMessageById(@PathVariable Long id) {
        Optional<Message> messageOptional = inboxService.getMessageById(id);
        Message message = messageOptional.get();
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @PostMapping("/message/create")
    public ResponseEntity createMessage(@RequestBody Message message) {
        try {
            inboxService.createMessage(message);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/message/update")
    public ResponseEntity updateMessage(@RequestBody Message message) {
        try {
            inboxService.createMessage(message);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/message/delete")
    public ResponseEntity deleteMessage(@PathVariable Long id) {
        inboxService.deleteMessage(id);
        return new ResponseEntity(HttpStatus.OK);
    }


}
