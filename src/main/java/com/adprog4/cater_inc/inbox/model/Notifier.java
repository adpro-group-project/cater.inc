package com.adprog4.cater_inc.inbox.model;

import com.adprog4.cater_inc.user.model.Admin;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "notifier")
public class Notifier implements Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    private Admin admin;

    @OneToMany
    private List<Inbox> inboxes;

    @OneToMany
    private List<Message> messages;

    public Notifier() {
        this.admin = null;
        this.inboxes = new ArrayList<>();
        this.messages = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public List<Inbox> getInboxes() {
        return inboxes;
    }

    public void setInboxes(List<Inbox> inboxes) {
        this.inboxes = inboxes;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    @Override
    public void registerInbox(Inbox inbox) {
        inboxes.add(inbox);
    }

    @Override
    public void removeInbox(Inbox inbox) {
        int currInbox = inboxes.indexOf(inbox);
        if (currInbox >= 0){
            inboxes.remove(currInbox);
        }
    }

    @Override
    public void notifyInbox(Message message) {
        for (Inbox inbox : inboxes) {
            inbox.update(message);
        }
    }

    public void addMessage(Message message){
        messages.add(message);
        notifyInbox(message);
    }
}
