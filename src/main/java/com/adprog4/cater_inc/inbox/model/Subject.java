package com.adprog4.cater_inc.inbox.model;

public interface Subject {
    public void registerInbox(Inbox inbox);
    public void removeInbox(Inbox inbox);
    public void notifyInbox(Message message);
}
