package com.adprog4.cater_inc.inbox.model;

public interface Observer {
    public void update(Message message);
}
