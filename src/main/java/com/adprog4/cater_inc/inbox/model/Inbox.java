package com.adprog4.cater_inc.inbox.model;

import com.adprog4.cater_inc.user.model.Customer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "inbox")
public class Inbox implements Observer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    private Customer customer;

    @ManyToMany
    private List<Message> inboxCustomer;

    public Inbox(){
        this.customer = null;
        this.inboxCustomer = new ArrayList<>();
    }

    @Override
    public void update(Message message) {
        inboxCustomer.add(message);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Message> getInboxCustomer() {
        return inboxCustomer;
    }

    public void setInboxCustomer(List<Message> inboxCustomer) {
        this.inboxCustomer = inboxCustomer;
    }
}
