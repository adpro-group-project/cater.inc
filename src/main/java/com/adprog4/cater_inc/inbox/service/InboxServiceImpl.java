package com.adprog4.cater_inc.inbox.service;

import com.adprog4.cater_inc.inbox.model.*;
import com.adprog4.cater_inc.inbox.repository.InboxRepository;
import com.adprog4.cater_inc.inbox.repository.MessageRepository;
import com.adprog4.cater_inc.inbox.repository.NotifierRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InboxServiceImpl implements InboxService{

    private final InboxRepository inboxRepository;
    private final MessageRepository messageRepository;
    private final NotifierRepository notifierRepository;

    public InboxServiceImpl(InboxRepository inboxRepository, MessageRepository messageRepository, NotifierRepository
                            notifierRepository) {
        this.inboxRepository = inboxRepository;
        this.messageRepository = messageRepository;
        this.notifierRepository = notifierRepository;
    }

    @Override
    public Optional<Notifier> getNotifierById(Long id) {
        return notifierRepository.findById(id);
    }

    @Override
    public Notifier createNotifier(Notifier notifier) {
        notifierRepository.save(notifier);
        return notifier;
    }

    @Override
    public Notifier updateNotifier(Notifier notifier) {
        notifierRepository.save(notifier);
        return notifier;
    }

    @Override
    public void deleteNotifier(Long id) {
        notifierRepository.deleteById(id);
    }

    @Override
    public void addInboxToNotifier(Long idNotifier, Long idInbox) {
        Notifier notifier = null;
        Optional<Notifier> notifierObject = notifierRepository.findById(idNotifier);
        if(notifierObject.isPresent()) {
            notifier = notifierObject.get();
        }

        Inbox inbox = null;
        Optional<Inbox> inboxObject = inboxRepository.findById(idInbox);
        if(inboxObject.isPresent()) {
            inbox = inboxObject.get();
        }

        notifier.registerInbox(inbox);
    }

    @Override
    public void addMessageToNotifier(Long idNotifier, Long idMessage) {
        Notifier notifier = null;
        Optional<Notifier> notifierObject = getNotifierById(idNotifier);
        if(notifierObject.isPresent()) {
            notifier = notifierObject.get();
        }

        Message message = null;
        Optional<Message> messageObject = getMessageById(idMessage);
        if(messageObject.isPresent()) {
            message = messageObject.get();
        }

        notifier.addMessage(message);
    }

    @Override
    public List<Inbox> getAllInboxes() {
        return inboxRepository.findAll();
    }

    @Override
    public Optional<Inbox> getInboxById(Long id) {
        return inboxRepository.findById(id);
    }

    @Override
    public List<Message> getAllMessagesInInbox(Long id) {
        Inbox inbox = null;
        Optional<Inbox> inboxObject = getInboxById(id);
        if(inboxObject.isPresent()) {
            inbox = inboxObject.get();
        }
        return inbox.getInboxCustomer();
    }

    @Override
    public Inbox createInbox(Inbox inbox) {
        inboxRepository.save(inbox);
        return inbox;
    }

    @Override
    public Inbox updateInbox(Inbox inbox) {
        inboxRepository.save(inbox);
        return inbox;
    }

    @Override
    public void deleteInbox(Long id) {
        inboxRepository.deleteById(id);
    }

    @Override
    public List<Message> getAllMessages() {
        return messageRepository.findAll();
    }

    @Override
    public Optional<Message> getMessageById(Long id) {
        return messageRepository.findById(id);
    }

    @Override
    public Message createMessage(Message message) {
        messageRepository.save(message);
        return message;
    }

    @Override
    public Message updateMessage(Message message) {
        messageRepository.save(message);
        return message;
    }

    @Override
    public void deleteMessage(Long id) {
        messageRepository.deleteById(id);
    }
}
