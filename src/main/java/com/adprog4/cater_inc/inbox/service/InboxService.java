package com.adprog4.cater_inc.inbox.service;

import com.adprog4.cater_inc.inbox.model.*;

import java.util.List;
import java.util.Optional;

public interface InboxService {
    // Notifier
    Optional<Notifier> getNotifierById(Long id);
    Notifier createNotifier(Notifier notifier);
    Notifier updateNotifier(Notifier notifier);
    void deleteNotifier(Long id);
    void addInboxToNotifier(Long idNotifier, Long idInbox);
    void addMessageToNotifier(Long idNotifier, Long idMessage);

    // Inbox
    List<Inbox> getAllInboxes();
    Optional<Inbox> getInboxById(Long id);
    List<Message> getAllMessagesInInbox(Long id);
    Inbox createInbox(Inbox inbox);
    Inbox updateInbox(Inbox inbox);
    void deleteInbox(Long id);

    // Message
    List<Message> getAllMessages();
    Optional<Message> getMessageById(Long id);
    Message createMessage(Message message);
    Message updateMessage(Message message);
    void deleteMessage(Long id);
}
