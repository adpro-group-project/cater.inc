package com.adprog4.cater_inc.inbox.repository;

import com.adprog4.cater_inc.inbox.model.Inbox;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InboxRepository extends JpaRepository<Inbox, Long> {
}
