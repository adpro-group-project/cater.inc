package com.adprog4.cater_inc.inbox.repository;

import com.adprog4.cater_inc.inbox.model.Notifier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotifierRepository extends JpaRepository<Notifier, Long> {
}
