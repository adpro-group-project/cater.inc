package com.adprog4.cater_inc.inbox.repository;

import com.adprog4.cater_inc.inbox.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
