package com.adprog4.cater_inc.pemesanan.factory.repository;


import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PemesananRepository extends JpaRepository<Pemesanan, Long> {

}
