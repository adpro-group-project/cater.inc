package com.adprog4.cater_inc.pemesanan.factory.controller;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.pemesanan.factory.model.Coupon;
import com.adprog4.cater_inc.pemesanan.factory.service.PemesananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/pemesanan")
public class PemesananController {

    @Autowired
    private PemesananService pemesananService;
     public PemesananController(PemesananService pemesananService){
         this.pemesananService = pemesananService;
     }

    @PostMapping("/checkout")
    public String checkoutPemesananWithCoupon(
            @RequestParam(value = "menu") Menu menu,
            @RequestParam(value = "coupon") Coupon coupon
    ){
         if(coupon == null){
             pemesananService.newPemesanan(menu);
         }
         else{
             pemesananService.newPemesanan(coupon, menu);
         }
        return "status/{idPemesanan}";
    }
}
