package com.adprog4.cater_inc.pemesanan.factory.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "coupon_type")
public abstract class Coupon {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    protected BigDecimal discountPrice;
    protected String description;

    public Coupon(BigDecimal discountPrice, String description){
        this.discountPrice = discountPrice;
        this.description = description;
    }

    public BigDecimal  getDiscountPrice(Pemesanan pemesanan){
        return this.discountPrice;
    }

    public void setDiscountPrice(){
        this.discountPrice = discountPrice;
    }

    public abstract BigDecimal getMenuPrice(Pemesanan pemesanan);
    public abstract String getDescription();
}
