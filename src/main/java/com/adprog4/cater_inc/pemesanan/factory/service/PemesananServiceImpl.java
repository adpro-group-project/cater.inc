package com.adprog4.cater_inc.pemesanan.factory.service;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.pemesanan.factory.model.Coupon;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import com.adprog4.cater_inc.pemesanan.factory.repository.PemesananRepository;
import org.springframework.stereotype.Service;
import sun.security.util.Pem;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Random;

@Service
public class PemesananServiceImpl implements PemesananService {

    private final PemesananRepository pemesananRepository;
    private final Random idGenerator;

    public PemesananServiceImpl(PemesananRepository pemesananRepository){
        this.pemesananRepository = pemesananRepository;
        this.idGenerator = new Random();
    }

    @Override
    public void newPemesanan(Coupon coupon, Menu menu) {
        Pemesanan pemesananBaru = new Pemesanan(coupon, menu);

        if(checkPemesananIdExist(pemesananBaru.getId())){
            long idPemesanan = generateId();
            while(checkPemesananIdExist(idPemesanan)){
                idPemesanan = generateId();
            }
            pemesananBaru.setId(idPemesanan);
        }
        pemesananRepository.save(pemesananBaru);
    }

    @Override
    public void newPemesanan(Menu menu) {
        Pemesanan pemesananBaru = new Pemesanan(menu);

        if(checkPemesananIdExist(pemesananBaru.getId())){
            long idPemesanan = generateId();
            while(checkPemesananIdExist(idPemesanan)){
                idPemesanan = generateId();
            }
            pemesananBaru.setId(idPemesanan);
        }
        pemesananRepository.save(pemesananBaru);
    }

    @Override
    public void setCoupon(long idPemesanan,Coupon coupon){
        Optional<Pemesanan> pemesanan = pemesananRepository.findById(idPemesanan);
        if(checkPemesananIdExist(idPemesanan)){
            Pemesanan pemesananSetCoupon = pemesanan.get();
            pemesananSetCoupon.setCoupon(coupon);
        }
    }

    @Override
    public Coupon getCoupon(long idPemesanan){
        Optional<Pemesanan> pemesanan = pemesananRepository.findById(idPemesanan);
        Coupon coupon = null;
        if(checkPemesananIdExist(idPemesanan)){
            Pemesanan pemesananGetCoupon = pemesanan.get();
            coupon = pemesananGetCoupon.getCouponCoupon();
        }
        return coupon;
    }

    @Override
    public BigDecimal getFoodPrice(long idPemesanan) {
        Optional<Pemesanan> pemesanan = pemesananRepository.findById(idPemesanan);
        BigDecimal foodPrice = BigDecimal.valueOf(0);
        if(checkPemesananIdExist(idPemesanan)){
            Pemesanan pemesananGetFoodPrice = pemesanan.get();
            foodPrice = pemesananGetFoodPrice.getFoodPrice();
        }
        return foodPrice;
    }

    @Override
    public BigDecimal getTotalPrice(long idPemesanan) {
        Optional<Pemesanan> pemesanan = pemesananRepository.findById(idPemesanan);
        BigDecimal totalPrice = BigDecimal.valueOf(0);
        if(checkPemesananIdExist(idPemesanan)){
            Pemesanan pemesananGetTotalPrice = pemesanan.get();
            totalPrice = pemesananGetTotalPrice.getPricePemesanan();
        }
        return totalPrice;
    }


    private boolean checkPemesananIdExist(long id){
        Optional<Pemesanan> pemesanan = pemesananRepository.findById(id);
        return pemesanan.isPresent();
    }

    private long generateId(){
        long random = idGenerator.nextInt();
        return Math.abs(random);
    }


}
