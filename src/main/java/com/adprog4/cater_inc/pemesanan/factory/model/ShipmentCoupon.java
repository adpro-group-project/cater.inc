package com.adprog4.cater_inc.pemesanan.factory.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity(name = "ShipmentCoupon")
@DiscriminatorValue("ShipmentCoupon")
public class ShipmentCoupon extends Coupon{

    public ShipmentCoupon(BigDecimal discountPrice, String description){
        super(discountPrice, description);
    }

    @Override
    public String getDescription(){
        return this.description;
    }

    @Override
    public BigDecimal getMenuPrice(Pemesanan pemesanan){
        return pemesanan.getFoodPrice().subtract(pemesanan.getShipmentPrice().subtract(this.discountPrice));
    }

}
