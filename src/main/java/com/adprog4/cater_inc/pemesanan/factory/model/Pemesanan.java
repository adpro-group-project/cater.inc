package com.adprog4.cater_inc.pemesanan.factory.model;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.statuspemesanan.model.*;
import com.adprog4.cater_inc.user.model.Customer;

import javax.persistence.*;
import javax.annotation.*;
import java.math.BigDecimal;

@Entity
@Table(name = "pemesanan")
public class Pemesanan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Transient
    private Coupon coupon;

    @Column(name = "food_price")
    private BigDecimal foodPrice;

    @Column(name = "shipment_Price")
    private BigDecimal shipmentPrice;

    @OneToOne
    private Menu menu;

    @ManyToOne
    private Customer customer;

    @OneToOne
    public StatusPemesanan currentState;

    public Pemesanan(Coupon coupon, Menu menu){
        this.coupon = coupon;
        if(menu.equals(null)){
            this.foodPrice = BigDecimal.valueOf(0);
        }
        this.foodPrice = menu.getPrice();
        this.shipmentPrice = BigDecimal.valueOf(10000);
        this.menu = menu;

        this.currentState = new PemesananMenungguKonfirmasi(this);

    }

    public Pemesanan(Menu menu){
        if(menu.equals(null)){
            this.foodPrice = BigDecimal.valueOf(0);
        }
        this.foodPrice = menu.getPrice();
        this.shipmentPrice = BigDecimal.valueOf(10000);
        this.menu = menu;
        this.menu = menu;

        this.currentState = new PemesananMenungguKonfirmasi(this);
    }

    public void setCoupon(Coupon coupon){
        this.coupon = coupon;
    }

    public Coupon getCouponCoupon(){
        return this.coupon;
    }

    public void setFoodPrice(BigDecimal foodPrice){
        this.foodPrice = foodPrice;
    }

    public BigDecimal getFoodPrice(){
        return this.foodPrice;
    }

    public void setShipmentPrice(BigDecimal shipmentPrice){
        this.shipmentPrice = shipmentPrice;
    }

    public BigDecimal getShipmentPrice(){
        return this.shipmentPrice;
    }

    public BigDecimal getPricePemesanan(){
        return coupon.getMenuPrice(this);
    }

    public BigDecimal getDiscountPrice(){
        return coupon.getDiscountPrice(this);
    }

    public String baru(){
        return this.currentState.buat();
    }

    public String konfirmasi(boolean kondisi){
        return this.currentState.konfirmasi(kondisi);
    }

    public String proses(){
        return this.currentState.proses();
    }

    public String kirim(){
        return this.currentState.kirim();
    }

    public String selesai(){
        return this.currentState.selesai();
    }

    public String gagal(){
        return this.currentState.gagal();
    }

    public void setStatus(StatusPemesanan status){
        this.currentState = status;
    }

    public StatusPemesanan getStatus(){
        return this.currentState;
    }

    public long getId(){
        return this.id;
    }

    public void setId(long id){
        this.id = id;
    }

}
