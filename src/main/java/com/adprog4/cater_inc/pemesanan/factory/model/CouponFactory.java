package com.adprog4.cater_inc.pemesanan.factory.model;

import java.math.BigDecimal;

public class CouponFactory {
    public static Coupon getCouponPrice(String couponType, BigDecimal discountPrice, String description){
        if (couponType == null){
            return null;
        }
        if(couponType.equalsIgnoreCase("Discount")) {
            return new FoodCoupon(discountPrice, description);
        }
        else if(couponType.equalsIgnoreCase("Shipment")) {
            return new ShipmentCoupon(discountPrice, description);
        }

        return null;
    }
}
