package com.adprog4.cater_inc.pemesanan.factory.service;


import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.pemesanan.factory.model.Coupon;

import java.math.BigDecimal;

public interface PemesananService {

    //Pemesanan
    public void newPemesanan(Coupon coupon, Menu menu);
    public void newPemesanan(Menu menu);
    public void setCoupon(long idPemesanan, Coupon coupon);
    public Coupon getCoupon(long idPemesanan);
    public BigDecimal getFoodPrice(long idPemesanan);
    public BigDecimal getTotalPrice(long idPemesanan);
}
