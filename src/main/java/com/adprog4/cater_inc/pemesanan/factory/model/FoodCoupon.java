package com.adprog4.cater_inc.pemesanan.factory.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity(name = "FoodCoupon")
@DiscriminatorValue("FoodCoupon")
public class FoodCoupon extends Coupon {

    public FoodCoupon(BigDecimal discountPrice, String description){
        super(discountPrice, description);
    }

    @Override
    public String getDescription(){
        return this.description;
    }

    @Override
    public BigDecimal getMenuPrice(Pemesanan pemesanan){
        BigDecimal  price = pemesanan.getFoodPrice();
        BigDecimal awal = price.subtract(price.multiply(discountPrice));
        return awal.add(pemesanan.getShipmentPrice());
    }

}
