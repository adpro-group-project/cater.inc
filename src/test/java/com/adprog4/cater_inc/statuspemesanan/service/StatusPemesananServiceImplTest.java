package com.adprog4.cater_inc.statuspemesanan.service;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import com.adprog4.cater_inc.pemesanan.factory.repository.PemesananRepository;
import com.adprog4.cater_inc.pemesanan.factory.service.PemesananService;
import com.adprog4.cater_inc.statuspemesanan.model.StatusPemesanan;
import com.adprog4.cater_inc.statuspemesanan.repository.StatusPemesananRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class StatusPemesananServiceImplTest {

    @InjectMocks
    private StatusPemesananServiceImpl statusPemesananService;
    private PemesananService pemesananService;

    @Mock
    private StatusPemesananRepository statusPemesananRepository;

    @Mock
    private PemesananRepository pemesananRepository;

    @Mock
    private Menu menu1;
    private Pemesanan pemesanan1;



    @BeforeEach
    void setUp(){
        Packet base1 = new Packet(2, "Paket Ayam Bakar", BigDecimal.valueOf(110000), null, 200);
        Packet base2 = new Packet(3, "Paket Ayam Rebus",BigDecimal.valueOf(100000), null, 200);
        menu1 = new Menu(0,base1);
        pemesanan1 = new Pemesanan(menu1);

    }

    @AfterEach
    void tearDown(){
        reset(statusPemesananRepository);
        reset(pemesananRepository);
    }

    @Test
    void testShowStatusPemesanan(){
        pemesanan1.setId(1);
        statusPemesananRepository.save(pemesanan1.getStatus());

        assertEquals(null, statusPemesananService.showStatusPemesanan(1));
    }
}
