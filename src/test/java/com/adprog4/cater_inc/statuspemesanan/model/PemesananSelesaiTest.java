package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.statuspemesanan.model.*;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class PemesananSelesaiTest {
    private PemesananSelesai pemesananSelesai;
    private Menu menu;
    private Packet base;

    @Mock
    private Pemesanan pemesanan;

    @BeforeEach
    public void setUp(){
        base = new Packet(2, "Paket Ayam Bakar",110000, null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(null, menu);
        pemesananSelesai = new PemesananSelesai(pemesanan);
        this.pemesanan.setStatus(pemesananSelesai);
    }

    @Test
    public void pemesananSelesaiTest(){
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Selesai");
    }

    @Test
    public void pemesananSelesaiBaruTest(){
        assertEquals(pemesanan.baru(), "[REJECT]: Pemesanan sudah selesai.");
    }

    @Test
    public void pemesananSelesaiKonfirmasiTest(){
        assertEquals(pemesanan.konfirmasi(true), "[REJECT]: Pemesanan sudah selesai.");
    }

    @Test
    public void pemesananSelesaiProsesTest(){
        assertEquals(pemesanan.proses(), "[REJECT]: Pemesanan sudah selesai.");
    }

    @Test
    public void pemesananSelesaiKirimTest(){
        assertEquals(pemesanan.kirim(), "[REJECT]: Pemesanan sudah selesai.");
    }

    @Test
    public void pemesananSelesaiSelesaiTest(){
        assertEquals(pemesanan.selesai(), "[REJECT]: Pemesanan sudah selesai.");
    }

    @Test
    public void pemesananSelesaiGagalTest(){
        assertEquals(pemesanan.gagal(), "[REJECT]: Pemesanan sudah selesai.");
    }


}
