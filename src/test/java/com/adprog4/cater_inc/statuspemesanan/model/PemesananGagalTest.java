package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.statuspemesanan.model.*;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class PemesananGagalTest {
    private PemesananGagal pemesananGagal;
    private Menu menu;
    private Packet base;

    @Mock
    private Pemesanan pemesanan;

    @BeforeEach
    public void setUp(){
        base = new Packet(2, "Paket Ayam Bakar",110000, null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(null,menu);
        pemesananGagal = new PemesananGagal(pemesanan);
        this.pemesanan.setStatus(pemesananGagal);
    }

    @Test
    public void pemesananGagalTest(){
        assertEquals(pemesanan.getStatus().status(), "Pemesanan gagal.");
    }

    @Test
    public void pemesananGagalBaruTest(){
        assertEquals(pemesanan.baru(), "[REJECT]: Pemesanan ini telah gagal dilakukan.");
    }

    @Test
    public void pemesananGagalKonfirmasiTest(){
        assertEquals(pemesanan.konfirmasi(true), "[REJECT]: Pemesanan ini telah gagal dilakukan.");
    }

    @Test
    public void pemesananGagalProsesTest(){
        assertEquals(pemesanan.proses(), "[REJECT]: Pemesanan ini telah gagal dilakukan.");
    }

    @Test
    public void pemesananGagalKirimTest(){
        assertEquals(pemesanan.kirim(), "[OKE]: Pemesanan akan dikirim ulang.");
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Di Kirim");
    }

    @Test
    public void pemesananGagalSelesaiTest(){
        assertEquals(pemesanan.selesai(), "[REJECT]: Pemesanan ini telah gagal dilakukan.");
    }

    @Test
    public void pemesananGagalGagalTest(){
        assertEquals(pemesanan.gagal(), "[REJECT]: Pemesanan ini telah gagal dilakukan.");
    }


}
