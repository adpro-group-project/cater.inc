package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.statuspemesanan.model.*;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class PemesananDiProsesTest {
    private PemesananDiProses pemesananDiProses;
    private Menu menu;
    private Packet base;

    @Mock
    private Pemesanan pemesanan;

    @BeforeEach
    public void setUp(){
        base = new Packet(2, "Paket Ayam Bakar",110000, null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(null, menu);
        pemesananDiProses = new PemesananDiProses(pemesanan);
        this.pemesanan.setStatus(pemesananDiProses);
    }

    @Test
    public void pemesananDiProsesTest(){
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Di Proses");
    }

    @Test
    public void pemesananDiProsesBaruTest(){
        assertEquals(pemesanan.baru(), "[REJECT]: Pemesanan tidak dapat dibuat lagi");
    }

    @Test
    public void pemesananDiProsesKonfirmasiTest(){
        assertEquals(pemesanan.konfirmasi(true), "[REJECT]: Pemesanan sudah dikonfirmasi");
    }

    @Test
    public void pemesananDiProsesProsesTest(){
        assertEquals(pemesanan.proses(), "[REJECT]: Pemesanan masih dalam tahap proses");
    }

    @Test
    public void pemesananDiProsesKirimTest(){
        assertEquals(pemesanan.kirim(), "[OKE]: Pemesanan dikirim ke alamat pembeli");
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Di Kirim");
    }

    @Test
    public void pemesananDiProsesSelesaiTest(){
        assertEquals(pemesanan.selesai(), "[REJECT] :Pemesanan masih dalam tahap proses");
    }

    @Test
    public void pemesananDiProsesGagalTest(){
        assertEquals(pemesanan.gagal(), "[REJECT]: Pemesanan tidak dapat gagal pada status ini");
    }

}
