package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import com.adprog4.cater_inc.statuspemesanan.model.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class PemesananDiKonfirmasiTest {
    private PemesananDiKonfirmasi pemesananDiKonfirmasi;
    private Menu menu;
    private Packet base;

    @Mock
    private Pemesanan pemesanan;

    @BeforeEach
    public void setUp(){
        base = new Packet(2, "Paket Ayam Bakar",110000, null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(null,menu);
        pemesananDiKonfirmasi = new PemesananDiKonfirmasi(pemesanan);
        this.pemesanan.setStatus(pemesananDiKonfirmasi);
    }

    @Test
    public void pemesananKosongTest(){
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Di Konfirmasi");
    }

    @Test
    public void pemesananDiKonfirmasiBaruTest(){
        assertEquals(pemesanan.baru(), "[REJECT]: Pemesanan tidak dapat dibuat lagi");
    }

    @Test
    public void pemesananDiKonfirmasiKonfirmasiTest(){
        assertEquals(pemesanan.konfirmasi(true), "[REJECT]: Pemesanan tidak dapat dikofirmasi lagi");
    }

    @Test
    public void pemesananDiKonfirmasiProsesTest(){
        assertEquals(pemesanan.proses(), "[OKE]: Pemesanan sedang diproses");
    }

    @Test
    public void pemesananDiKonfirmasiKirimTest(){
        assertEquals(pemesanan.kirim(), "[REJECT]: Pemesanan masih dalam tahap proses");
    }

    @Test
    public void pemesananDiKonfirmasiSelesaiTest(){
        assertEquals(pemesanan.selesai(), "[REJECT] :Pemesanan masih dalam tahap proses");
    }

    @Test
    public void pemesananDiKonfirmasiGagaluTest(){
        assertEquals(pemesanan.gagal(), "[REJECT]: Pemesanan tidak dapat gagal pada status ini");
    }
}
