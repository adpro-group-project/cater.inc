package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.statuspemesanan.model.*;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class PemesananMenungguKonfirmasiTest {
    private PemesananMenungguKonfirmasi pemesananMenungguKonfirmasi;
    private Menu menu;
    private Packet base;

    @Mock
    private Pemesanan pemesanan;

    @BeforeEach
    public void setUp(){
        base = new Packet(2, "Paket Ayam Bakar",110000, null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(null,menu);
        pemesananMenungguKonfirmasi = new PemesananMenungguKonfirmasi(pemesanan);
        this.pemesanan.setStatus(pemesananMenungguKonfirmasi);
    }

    @Test
    public void pemesananMenungguKonfirmasiTest(){
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Menunggu Konfirmasi");
    }

    @Test
    public void pemesananMenungguKonfirmasiBaruTest(){
        assertEquals(pemesanan.baru(), "[REJECT]: Pemesanan tidak dapat dibuat lagi");
    }

    @Test
    public void pemesananMenungguKonfirmasiKonfirmasiTrueTest(){
        assertEquals(pemesanan.konfirmasi(true), "[OKE]: Pemesanan dikonfirmasi");
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Di Proses");
    }

    public void pemesananMenungguKonfirmasiKonfirmasiFalseTest(){
        assertEquals(pemesanan.konfirmasi(false), "[OKE]: Pemesanan ditolak");
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Selesai");
    }

    @Test
    public void pemesananMenungguKonfirmasiProsesTest(){
        assertEquals(pemesanan.proses(), "[REJECT]: Pemesanan belum dikonfirmasi");
    }

    @Test
    public void pemesananMenungguKonfirmasiKirimTest(){
        assertEquals(pemesanan.kirim(), "[REJECT]: Pemesanan belum dikonfirmasi");
    }

    @Test
    public void pemesananMenungguKonfirmasiSelesaiTest(){
        assertEquals(pemesanan.selesai(), "[REJECT]: Pemesanan belum dikonfirmasi");
    }

    @Test
    public void pemesananMenungguKonfirmasiGagalTest(){
        assertEquals(pemesanan.gagal(), "[REJECT]: Pemesanan tidak dapat gagal pada status ini");
    }

}
