package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.statuspemesanan.model.*;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class PemesananKosongTest {
    private PemesananKosong pemesananKosong;
    private Menu menu;
    private Packet base;

    @Mock
    private Pemesanan pemesanan;

    @BeforeEach
    public void setUp(){
        base = new Packet(2, "Paket Ayam Bakar",110000, null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(null,menu);
        pemesananKosong = new PemesananKosong(pemesanan);
        this.pemesanan.setStatus(pemesananKosong);
    }

    @Test
    public void pemesananKosongTest(){
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Kosong");
    }

    @Test
    public void pemesananKosongBaruTest(){
        assertEquals(pemesanan.baru(), "[OKE]: Pesanan Dibuat");
    }

    @Test
    public void pemesananKosongKonfirmasiTest(){
        assertEquals(pemesanan.konfirmasi(true), "[REJECT]: Pesanan belum dibuat");
    }

    @Test
    public void pemesananKosongProsesTest(){
        assertEquals(pemesanan.proses(), "[REJECT]: Pesanan belum dibuat");
    }

    @Test
    public void pemesananKosongKirimTest(){
        assertEquals(pemesanan.kirim(), "[REJECT]: Pesanan belum dibuat");
    }

    @Test
    public void pemesananKosongSelesaiTest(){
        assertEquals(pemesanan.selesai(), "[REJECT]: Pesanan belum dibuat");
    }

    @Test
    public void pemesananKosongGagalTest(){
        assertEquals(pemesanan.gagal(), "[REJECT]: Pemesanan tidak dapat gagal pada status ini");
    }

}
