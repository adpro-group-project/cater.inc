package com.adprog4.cater_inc.statuspemesanan.model;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import com.adprog4.cater_inc.statuspemesanan.model.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class PemesananDiKirimTest {
    private PemesananDiKirim pemesananDiKirim;
    private Menu menu;
    private Packet base;

    @Mock
    private Pemesanan pemesanan;

    @BeforeEach
    public void setUp(){
        base = new Packet(2, "Paket Ayam Bakar",110000, null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(null,menu);
        pemesananDiKirim = new PemesananDiKirim(pemesanan);
        this.pemesanan.setStatus(pemesananDiKirim);
    }

    @Test
    public void pemesananDiKirimTest(){
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Di Kirim");
    }

    @Test
    public void pemesananDiKirimBaruTest(){
        assertEquals(pemesanan.baru(), "[REJECT]: Pemesanan tidak dapat dibuat lagi");
    }

    @Test
    public void pemesananDiKirimKonfirmasiTest(){
        assertEquals(pemesanan.konfirmasi(true), "[REJECT]: Pemesanan tidak dapat dikofirmasi lagi");
    }

    @Test
    public void pemesananDiKirimProsesTest(){
        assertEquals(pemesanan.proses(), "[REJECT]: Pemesanan sudah dalam tahap pengiriman");
    }

    @Test
    public void pemesananDiKirimKirimTest(){
        assertEquals(pemesanan.kirim(), "[REJECT]: Pemesanan sudah dalam tahap pengiriman");
    }

    @Test
    public void pemesananDiKirimSelesaiTest(){
        assertEquals(pemesanan.selesai(), "[OKE]: Pemesanan sudah sampai dialamat pembeli");
        assertEquals(pemesanan.getStatus().status(), "Pemesanan Selesai");
    }

    @Test
    public void pemesananDiKirimGagalTest(){
        assertEquals(pemesanan.gagal(), "[OKE]: Pemesanan gagal dalam pengiriman");
        assertEquals(pemesanan.getStatus().status(), "Pemesanan gagal.");
    }

}