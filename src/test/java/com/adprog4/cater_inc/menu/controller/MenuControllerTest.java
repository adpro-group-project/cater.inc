package com.adprog4.cater_inc.menu.controller;

import com.adprog4.cater_inc.auth.OAuthUtils;
import com.adprog4.cater_inc.menu.model.*;
import com.adprog4.cater_inc.menu.service.MenuServiceimpl;


import static com.adprog4.cater_inc.auth.OAuthUtils.getOauthAuthenticationFor;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.InvalidObjectException;
import java.math.BigDecimal;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializable;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.hamcrest.Matchers.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MenuController.class)
class MenuControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    MenuServiceimpl menuService;

    @MockBean
    private ClientRegistrationRepository clientRegistrationRepository;

    private OAuth2User principal;

    @BeforeEach
    void setUpUser(){
        principal = OAuthUtils.createOAuth2User(
                "Mark Hoogenboom", "mark.hoogenboom@example.com");

    }


    @AfterEach
    void tearDown(){
        reset(menuService);
    }


    @Test
    void findAllProducts() throws Exception {
        Product product = new CustomPacket(1);
        Product product1 = new Packet(2, "asal", BigDecimal.valueOf(1200.0),null,1);
        List<Product> productList = new ArrayList<>();
        productList.add(product);
        productList.add(product1);

        when(menuService.getAllProducts()).thenReturn(productList);

        mockMvc.perform(MockMvcRequestBuilders.get("/menu/prod")
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].@class",is("CustomPacket")))
                .andExpect(jsonPath("$[0].name", is("Custom")))
                .andExpect(jsonPath("$[0].price", is(0)))
                .andExpect(jsonPath("$[1].@class",is("Packet")))
                .andExpect(jsonPath("$[1].name", is("asal")))
                .andExpect(jsonPath("$[1].price", is(1200.0)));

    }

    @Test
    void findAllTopping() throws Exception {
        Topping topping = new Food(3, "Ayam Krim", BigDecimal.valueOf(12000.0),1);
        Topping topping1 = new Beverage(4, "Teh Bakar",
                BigDecimal.valueOf(5000.0),1);

        List<Topping> toppingList = new ArrayList<>();
        toppingList.add(topping);
        toppingList.add(topping1);

        when(menuService.getAllToppings()).thenReturn(toppingList);

        mockMvc.perform(MockMvcRequestBuilders.get("/menu/top")
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].@class",is("Food")))
                .andExpect(jsonPath("$[0].name", is("Ayam Krim")))
                .andExpect(jsonPath("$[0].price", is(12000.0)))
                .andExpect(jsonPath("$[0].quantity", is(1)))
                .andExpect(jsonPath("$[1].@class",is("Beverage")))
                .andExpect(jsonPath("$[1].name", is("Teh Bakar")))
                .andExpect(jsonPath("$[1].price", is(5000.0)))
                .andExpect(jsonPath("$[1].quantity", is(1)));
    }

    @Test
    void findProductByNameFound() throws Exception {
        Product product = new CustomPacket(1);
        when(menuService.findProductByName("Custom")).thenReturn(Optional.of(product));

        mockMvc.perform(MockMvcRequestBuilders.get("/menu/prod/Custom")
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.@class",is("CustomPacket")))
                .andExpect(jsonPath("$.name", is("Custom")))
                .andExpect(jsonPath("$.price", is(0)));


    }

    @Test
    void findProductByNameNotFound() throws Exception {
        when(menuService.findProductByName("Custom")).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.get("/menu/prod/Custom")
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    void findToppingByNameFound() throws Exception {
        Topping topping = new Food(1, "Ayam Setan",2000,1);
        when(menuService.findToppingByName("Ayam Setan")).thenReturn(Optional.of(topping));
        mockMvc.perform(MockMvcRequestBuilders.get("/menu/top/Ayam Setan")
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.@class",is("Food")))
                .andExpect(jsonPath("$.name", is("Ayam Setan")))
                .andExpect(jsonPath("$.price", is(2000.0)));
    }

    @Test
    void findToppingByNameNotFound() throws Exception {
        when(menuService.findToppingByName("Ayam Setan")).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.get("/menu/top/Ayam Setan")
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    void checkMenuPrice() throws Exception {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Product base = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        MenuDTO menuDTO = new MenuDTO(1,BigDecimal.valueOf(32000.0), base,
                base, new HashMap<String, Integer>());

        when(menuService.checkMenuPrice(Mockito.any(MenuDTO.class)))
                .thenReturn(BigDecimal.valueOf(32000.0));

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/price")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(menuDTO));

        mockMvc.perform(builder).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("32000.0"));

    }

    @Test
    void addToppingValid() throws Exception {
        ToppingDTO toppingDTO = new ToppingDTO("Food",1,"Bebek Bakar",
                BigDecimal.valueOf(12000.0), 4);

        doNothing().when(menuService).addTopping(Mockito.any(ToppingDTO.class));

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/add/top")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(toppingDTO));

        mockMvc.perform(builder).andExpect(status().isOk());

        verify(menuService,times(1))
                .addTopping(Mockito.any(ToppingDTO.class));
    }

    @Test
    void addToppingNotValid() throws Exception {
        ToppingDTO toppingDTO = new ToppingDTO("Food",1,"Bebek Bakar",
                BigDecimal.valueOf(12000.0), 4);

        doThrow(DataIntegrityViolationException.class)
                .when(menuService)
                .addTopping(Mockito.any(ToppingDTO.class));

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/add/top")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(toppingDTO));

        mockMvc.perform(builder).andExpect(status().isBadRequest());

        verify(menuService,times(1))
                .addTopping(Mockito.any(ToppingDTO.class));
    }

    @Test
    void addProductValid() throws Exception {
        ProductDTO productDTO = new ProductDTO("CustomPacket",1,
                "Custom", BigDecimal.ZERO,null,1);

        doNothing().when(menuService).addProduct(Mockito.any(ProductDTO.class));

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/add/prod")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(productDTO));

        mockMvc.perform(builder).andExpect(status().isOk());

        verify(menuService, times(1))
                .addProduct(Mockito.any(ProductDTO.class));
    }

    @Test
    void addProductNotValid() throws Exception {
        ProductDTO productDTO = new ProductDTO("CustomPacket",1,
                "Custom", BigDecimal.ZERO,null,1);

        doThrow(DataIntegrityViolationException.class).when(menuService).addProduct(Mockito.any(ProductDTO.class));

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/add/prod")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(productDTO));

        mockMvc.perform(builder).andExpect(status().isBadRequest());

        verify(menuService, times(1))
                .addProduct(Mockito.any(ProductDTO.class));
    }

    @Test
    void findAllMenu() throws Exception {
        Menu menu = new Menu(1, new CustomPacket(1));
        List<Menu> menuList = new ArrayList<>();
        menuList.add(menu);

        when(menuService.getAllMenu()).thenReturn(menuList);

        mockMvc.perform(MockMvcRequestBuilders.get("/menu")
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id",is(1)))
                .andExpect(jsonPath("$[0].product.@class",is("CustomPacket")))
                .andExpect(jsonPath("$[0].price", is(0)));


    }

    @Test
    void addMenuValid() throws Exception {
        CustomPacket customPacket = new CustomPacket(1);
        MenuDTO menuDTO = new MenuDTO(1, BigDecimal.ZERO,customPacket,customPacket,null);
       when(menuService.addMenu(Mockito.any(MenuDTO.class))).thenReturn(menuDTO.convertToMenu());

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/add/menu")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(menuDTO));

        mockMvc.perform(builder).andExpect(status().isOk());

        verify(menuService, times(1))
                .addMenu(Mockito.any(MenuDTO.class));

    }

    @Test
    void addMenuNotValid() throws Exception {
        CustomPacket customPacket = new CustomPacket(1);
        MenuDTO menuDTO = new MenuDTO(1, BigDecimal.ZERO,customPacket,customPacket,null);
        doThrow(InvalidObjectException.class).when(menuService).addMenu(Mockito.any(MenuDTO.class));

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/add/menu")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(menuDTO));

        mockMvc.perform(builder).andExpect(status().isBadRequest());

        verify(menuService, times(1))
                .addMenu(Mockito.any(MenuDTO.class));
    }

    @Test
    void menuSetBase() throws Exception {
        Menu menu = new Menu(1,new CustomPacket(1));
        Packet packet = new Packet(2, "asal",12000,null,200);
        MenuProductWrapper menuProductWrapper = new MenuProductWrapper(menu, packet);
        Menu newMenu = new Menu(1,packet);

        when(menuService.menuSetBase(Mockito.any(MenuProductWrapper.class)))
                .thenReturn(newMenu);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/menu/base")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(menuProductWrapper));

        mockMvc.perform(builder).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.base.@class", is("Packet")))
                .andExpect(jsonPath("$.base.name", is("asal")))
                .andExpect(jsonPath("$.base.price", is(12000.0)));
    }

    @Test
    void menuAddTopping() throws Exception {
        Menu menu = new Menu(1,new CustomPacket(1));
        Beverage beverage =  new Beverage(2,"Teh Bakar",10000,1);
        MenuToppingWrapper menuToppingWrapper = new MenuToppingWrapper(menu, beverage);
        Menu newMenu = new Menu(1, new CustomPacket(1));
        newMenu.addTopping(beverage);

        when(menuService.menuAddTopping(Mockito.any(MenuToppingWrapper.class)))
                .thenReturn(newMenu);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/menu/add/top")
                .with(csrf())
                .with(authentication(getOauthAuthenticationFor(principal)))
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(menuToppingWrapper));

        mockMvc.perform(builder).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.product.@class", is("Beverage")))
                .andExpect(jsonPath("$.product.name", is("Teh Bakar")))
                .andExpect(jsonPath("$.product.price", is(10000.0)))
                .andExpect(jsonPath("$.product.quantity", is(1)));

    }

    @Test
    void menuRemoveTopping() throws Exception {
        Menu menu = new Menu(1,new CustomPacket(1));
        Beverage beverage =  new Beverage(2,"Teh Bakar",10000,1);
        MenuToppingWrapper menuToppingWrapper = new MenuToppingWrapper(menu, beverage);
        Menu newMenu = new Menu(1, new CustomPacket(1));

        when(menuService.menuRemoveTopping(Mockito.any(MenuToppingWrapper.class)))
                .thenReturn(newMenu);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/menu/menu/rm/top")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .with(authentication(getOauthAuthenticationFor(principal)))
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(menuToppingWrapper));

        mockMvc.perform(builder).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.product.@class", is("CustomPacket")))
                .andExpect(jsonPath("$.product.name", is("Custom")))
                .andExpect(jsonPath("$.product.price", is(0)));
    }

}