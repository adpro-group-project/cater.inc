package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ProductDTOTest {
    ProductDTO productDTO;
    ProductDTO productDTO1;
    Map<String, Integer> content;

    @BeforeEach
    void setUp() {
        content = new HashMap<>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        productDTO = new ProductDTO("Packet", 2,
                "Paket Ayam Bakar", BigDecimal.valueOf(32000.0),
                content, 200);
        productDTO1 = new ProductDTO("CustomPacket", 3,
                "Custom", null, null, 0);
    }

    @Test
    void convertToProduct() {
        Packet packet = new Packet(2,
                "Paket Ayam Bakar", BigDecimal.valueOf(32000.0),
                content, 200);
        CustomPacket customPacket = new CustomPacket(3);
        assertTrue(packet.equals(productDTO.convertToProduct()));
        assertTrue(customPacket.equals(productDTO1.convertToProduct()));
    }


}