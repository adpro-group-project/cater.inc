package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class BeverageTest {
    Beverage beverage;

    @BeforeEach
    void setUp(){
        beverage = new Beverage(2, "Teh Manis", 4000, 1);
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Packet packet = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        beverage.setProduct(packet);
    }

    @Test
    void testBeverageConstruct(){
        Beverage beverage1 = new Beverage(2, "Teh Manis", BigDecimal.valueOf(4000.0), 1);
        Beverage beverage2 = new Beverage(beverage);
        assertTrue(beverage.equals(beverage1));
        assertTrue(beverage.equals(beverage2));

        beverage1 = new Beverage();
        assertTrue(beverage1.getPrice() == null);
    }

    @Test
    void getProduct() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Packet packet1 = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        assertTrue(packet1.equals(beverage.getProduct()));

    }

    @Test
    void setProduct() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Packet packet1 = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        beverage.setProduct(packet1);
        assertTrue(packet1.equals(beverage.getProduct()));
    }

    @Test
    void getId() {
        assertEquals(2, beverage.getId());
    }

    @Test
    void setId() {
        beverage.setId(3);
        assertEquals(3, beverage.getId());
    }

    @Test
    void getName() {
        assertEquals("Teh Manis", beverage.getName());
    }

    @Test
    void setName() {
        beverage.setName("Kopi Susu");
        assertEquals("Kopi Susu", beverage.getName());
    }

    @Test
    void getPrice() {
        assertEquals(BigDecimal.valueOf(4000.0), beverage.getPrice());
    }

    @Test
    void setPrice() {
        beverage.setPrice(BigDecimal.valueOf(5000.0));
        assertEquals(BigDecimal.valueOf(5000.0), beverage.getPrice());
    }

    @Test
    void getRealPrice(){
        assertEquals(BigDecimal.valueOf(36000.0), beverage.getRealPrice());
    }

    @Test
    void testEquals() {
        Beverage beverage1 = new Beverage(2, "Teh Manis", 4000, 1);
        assertTrue(beverage.equals(beverage1));

        Food food1 = new Food(2, "Teh Manis", 1,4000);
        assertFalse(beverage.equals(food1));

    }



}