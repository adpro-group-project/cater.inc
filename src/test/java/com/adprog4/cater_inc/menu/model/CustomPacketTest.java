package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class CustomPacketTest {
    CustomPacket customPacket;

    @BeforeEach
    void setUp() {
        customPacket = new CustomPacket(2);
    }

    @Test
    void getId() {
        assertEquals(2, customPacket.getId());
    }

    @Test
    void customPacketConstruct(){
        CustomPacket custom1 = new CustomPacket();
        assertTrue(custom1.getPrice() == null);
    }

    @Test
    void setId() {
        customPacket.setId(5);
        assertEquals(5, customPacket.getId());

    }

    @Test
    void testGetName() {
        assertEquals("Custom", customPacket.getName());
    }

    @Test
    void testSetName() {
        customPacket.setName("Custom2");
        assertEquals("Custom2", customPacket.getName());
    }

    @Test
    void testGetPrice() {
        assertEquals(BigDecimal.ZERO, customPacket.getPrice());
    }

    @Test
    void testSetPrice() {
        customPacket.setPrice(BigDecimal.valueOf(24000));
        assertEquals(BigDecimal.valueOf(24000), customPacket.getPrice());
    }

    @Test
    void testEquals() {
        CustomPacket customPacket1 = new CustomPacket(2);
        Product customPacket2 = new CustomPacket(3);
        Packet packet = new Packet(3,"Ayam", BigDecimal.valueOf(2000), new HashMap<>(), 20);
        assertTrue(customPacket1.equals(customPacket));
        assertTrue(customPacket2.equals(customPacket));
        assertTrue(customPacket1.equals(customPacket2));
        assertFalse(customPacket.equals(packet));

    }



}