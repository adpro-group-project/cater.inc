package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ToppingDTOTest {

    ToppingDTO toppingDTO;
    ToppingDTO toppingDTO1;

    @BeforeEach
    void setUp() {
        toppingDTO = new ToppingDTO("Food", 2,
                "Ayam Madu", BigDecimal.valueOf(12000.0),
                2);

        toppingDTO1 = new ToppingDTO("Beverage", 2,
                "Teh Bakar", BigDecimal.valueOf(12000.0),
                2);
    }

    @Test
    void convertToTopping() {
        Food food = new Food(2,
                "Ayam Madu", BigDecimal.valueOf(12000.0),
                2);
        Beverage beverage = new Beverage(2,
                "Teh Bakar", BigDecimal.valueOf(12000.0),
                2);
        assertTrue(beverage.equals(toppingDTO1.convertToTopping()));
        assertTrue(food.equals(toppingDTO.convertToTopping()));
    }


}