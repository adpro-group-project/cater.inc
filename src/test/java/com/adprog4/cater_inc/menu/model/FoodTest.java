package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class FoodTest {
    Food food;

    @BeforeEach
    void setUp() {
        food = new Food(2, "Ayam Bakar", 12000,1);
    }


    @Test
    void testFoodConstruct(){
        Food food1 = new Food(2, "Ayam Bakar", BigDecimal.valueOf(12000.0),1);
        Food food2 = new Food(food);
        assertTrue(food.equals(food1));
        assertTrue(food.equals(food2));
        food1 = new Food();
        assertTrue(food1.getPrice() == null);
    }

    @Test
    void getId() {
        assertEquals(2, food.getId());
    }

    @Test
    void setId() {
        food.setId(3);
        assertEquals(3, food.getId());
    }

    @Test
    void getName() {
        assertEquals("Ayam Bakar", food.getName());
    }

    @Test
    void setName() {
        food.setName("Bebek Bakar");
        assertEquals("Bebek Bakar", food.getName());
    }

    @Test
    void testGetPrice() {
        assertEquals(BigDecimal.valueOf(12000.0), food.getPrice());
    }

    @Test
    void setPrice() {
        food.setPrice(BigDecimal.valueOf(15000.0));
        assertEquals(BigDecimal.valueOf(15000.0), food.getPrice());
    }

    @Test
    void getRealPrice(){
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Packet packet = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        food.setProduct(packet);
        assertEquals(BigDecimal.valueOf(44000.0), food.getRealPrice());
    }

    @Test
    void getAndSetProduct(){
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Packet packet = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        food.setProduct(packet);
        assertEquals(packet, food.getProduct());
    }

    @Test
    void testEquals() {
        Food food1 = new Food(2, "Ayam Bakar", 12000,1);
        assertTrue(food.equals(food1));

        Beverage beverage = new Beverage(2, "Ayam Bakar", 12000, 1);
        assertFalse(food.equals(beverage));
    }


}