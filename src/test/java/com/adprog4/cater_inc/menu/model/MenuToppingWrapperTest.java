package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MenuToppingWrapperTest {
    MenuToppingWrapper menuTop;

    @BeforeEach
    void setUp() {
        Menu menu = new Menu(4, new CustomPacket(2));
        Food food = new Food(3, "Ayam Bakar", 12000, 2);
        menuTop = new MenuToppingWrapper(menu, food);
    }

    @Test
    void getMenu() {
        assertTrue(menuTop.getMenu().getBase() instanceof CustomPacket);
    }

    @Test
    void setMenu() {
        Menu menu = new Menu(4, new CustomPacket(2));
        menuTop.setMenu(menu);
        assertTrue(menuTop.getMenu().equals(menu));
    }

    @Test
    void getTopping() {
        assertTrue(menuTop.getTopping() instanceof Food);
    }

    @Test
    void setTopping() {
        Food food = new Food(3, "Ayam Goreng", 10000, 2);
        menuTop.setTopping(food);
        assertTrue(menuTop.getTopping().equals(food));
    }


}