package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MenuProductWrapperTest {

    MenuProductWrapper menuProd;

    @BeforeEach
    void setUp(){
        Menu menu = new Menu(4, new CustomPacket(2));
        Product customPacket = new CustomPacket(2);
        menuProd = new MenuProductWrapper(menu, customPacket);
    }

    @Test
    void getMenu(){
        assertTrue(menuProd.getMenu().getBase() instanceof CustomPacket);
    }

    @Test
    void setMenu() {
        Menu menu = new Menu(4, new CustomPacket(2));
        menuProd.setMenu(menu);
        assertTrue(menuProd.getMenu().equals(menu));
    }

    @Test
    void getProduct() {
        assertTrue(menuProd.getProduct() instanceof CustomPacket);
    }

    @Test
    void setProduct() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Packet packet = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        menuProd.setProduct(packet);
        assertTrue(menuProd.getProduct().equals(packet));
    }
}