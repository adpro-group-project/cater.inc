package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MenuDTOTest {

    MenuDTO menuDTO;

    @BeforeEach
    void setUp() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Product base = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        menuDTO = new MenuDTO(2, base.getPrice(), base, base, new HashMap<String, Integer>());

    }

    @Test
    void convertDTOMenu(){
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Product base = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        MenuDTO menuDTO1 = new MenuDTO(new Menu(2, base));

        assertTrue(menuDTO.getId() == menuDTO1.getId());
        assertTrue(menuDTO.getPrice().equals(menuDTO1.getPrice()));
        assertTrue(menuDTO.getProduct().equals(menuDTO1.getProduct()));
        assertTrue(menuDTO.getBase().equals(menuDTO1.getBase()));
        assertTrue(menuDTO.getContent().equals(menuDTO1.getContent()));


    }

    @Test
    void convertToMenu() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Product base = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        Menu menu = new Menu(2, base);
        Menu menuAssert = menuDTO.convertToMenu();

        assertTrue(menu.getId() == menu.getId());
        assertTrue(menu.getPrice().equals(menuAssert.getPrice()));
        assertTrue(menu.getProduct().equals(menuAssert.getProduct()));
        assertTrue(menu.getBase().equals(menuAssert.getBase()));
        assertTrue(menu.getContent().equals(menuAssert.getContent()));
    }


    @Test
    void getId() {
        assertEquals(2, menuDTO.getId());
    }

    @Test
    void getPrice() {
        assertEquals(BigDecimal.valueOf(32000.0), menuDTO.getPrice());
    }

    @Test
    void getProduct() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Product base = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        assertEquals(base, menuDTO.getProduct());

    }

    @Test
    void getBase() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Product base = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        assertEquals(base, menuDTO.getBase());
    }

    @Test
    void getContent() {
        assertTrue(new HashMap<String, Integer>().equals(menuDTO.getContent()));
    }

    @Test
    void setId() {
        menuDTO.setId(3);
        assertEquals(3, menuDTO.getId());

    }

    @Test
    void setPrice() {
        menuDTO.setPrice(BigDecimal.valueOf(3000.0));
        assertEquals(BigDecimal.valueOf(3000.0), menuDTO.getPrice());
    }

    @Test
    void setProduct() {
        menuDTO.setProduct(new CustomPacket(2));
        assertTrue(new CustomPacket(2).equals(menuDTO.getProduct()));
    }

    @Test
    void setBase() {
        menuDTO.setBase(new CustomPacket(2));
        assertTrue(new CustomPacket(2).equals(menuDTO.getBase()));
    }

    @Test
    void setContent() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        menuDTO.setContent(content);
        assertTrue(content.equals(menuDTO.getContent()));

    }
}