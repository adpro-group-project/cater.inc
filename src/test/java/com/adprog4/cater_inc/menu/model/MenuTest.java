package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MenuTest {

    Menu menu;
    Product base;

    @BeforeEach
    void setUp() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        base = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        menu = new Menu(2, base);
    }

    @Test
    void getId(){
        assertEquals(2, menu.getId());
    }

    @Test
    void setId(){
        menu.setId(4);
        assertEquals(4, menu.getId());
    }


    @Test
    void getPrice() {
        assertEquals(BigDecimal.valueOf(32000.0), menu.getPrice());
    }

    @Test
    void setPrice() {
        menu.setPrice(BigDecimal.valueOf(22000));
        assertEquals(BigDecimal.valueOf(22000), menu.getPrice());
    }

    @Test
    void addTopping(){
        Topping topping = new Food(2, "Ayam Bakar", 12000,1);
        Topping topping1 = new Food(2, "Bebek Bakar", 15000,1);

        //Variant Topping
        menu.addTopping(topping);
        menu.addTopping(topping1);
        assertEquals(BigDecimal.valueOf(59000.0), menu.getPrice());

        //Same Topping
        Topping topping2 = new Food((Food)topping);
        menu.addTopping(topping2);
        assertEquals(BigDecimal.valueOf(71000.0), menu.getPrice());
    }

    @Test
    void removeTopping() {
        Topping topping = new Food(2, "Ayam Bakar", 12000,1);
        Topping topping1 = new Food(2, "Bebek Bakar", 15000,1);
        menu.addTopping(topping);
        menu.addTopping(topping1);

        //First Topping
        menu.removeTopping(topping);
        assertEquals(BigDecimal.valueOf(47000.0), menu.getPrice());

        //lastTopping
        menu.removeTopping(topping1);
        assertEquals(BigDecimal.valueOf(32000.0), menu.getPrice());


    }
    @Test
    void setBase(){
        Topping topping = new Food(2, "Ayam Bakar", 12000,1);
        Topping topping1 = new Food(2, "Bebek Bakar", 15000,1);
        Product newBase = new CustomPacket(2);

        //Without Topping
        menu.setBase(newBase);
        assertEquals(BigDecimal.ZERO, menu.getPrice());


        // With Topping
        menu.setBase(base);
        menu.addTopping(topping);
        menu.addTopping(topping1);
        menu.setBase(newBase);
        assertTrue(menu.getBase().equals(newBase));
        assertEquals(BigDecimal.valueOf(27000.0), menu.getPrice());
    }

    @Test
    void getBase() {
        assertTrue(base.equals(menu.getBase()));
    }

    @Test
    void getProduct() {
        assertTrue(menu.getProduct().equals(base));
    }

    @Test
    void setProduct() {
        menu.setProduct(new CustomPacket(2));
        assertTrue(menu.getProduct().equals(new CustomPacket(2)));
    }

    @Test
    void getContent() {
        assertTrue(menu.getContent().isEmpty());
    }

    @Test
    void setContent() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        menu.setContent(content);
        assertTrue(menu.getContent().equals(content));
    }
}