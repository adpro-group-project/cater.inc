package com.adprog4.cater_inc.menu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;



class PacketTest {
    Packet packet;

    @BeforeEach
    void setUp() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        packet = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
    }



    @Test
    void getId() {
        assertEquals(2, packet.getId());
    }

    @Test
    void setId() {
        packet.setId(3);
        assertEquals(3, packet.getId());
    }

    @Test
    void TestPacket(){
        Packet packet1 = new Packet(packet);
        assertTrue(packet.equals(packet1));

        packet1 = new Packet();
        assertTrue(packet1.getContent() == null);
        assertTrue(packet1.getPrice() == null);
    }

    @Test
    void getName() {
        assertEquals("Paket Ayam Bakar", packet.getName());
    }

    @Test
    void setName() {
        packet.setName("Paket Bebek Bakar");
        assertEquals("Paket Bebek Bakar", packet.getName());
    }

    @Test
    void getPrice() {
        assertEquals(BigDecimal.valueOf(32000.0), packet.getPrice());
    }

    @Test
    void setPrice(){
        packet.setPrice(BigDecimal.valueOf(40000));
        assertEquals(BigDecimal.valueOf(40000), packet.getPrice());
    }

    @Test
    void testEqualsProduct() {
        Product product = new Food(2, "Paket Ayam Bakar", 1,32000 );
        assertFalse(packet.equals(product));
    }

    @Test
    void testEqualsPacket(){
        Map<String, Integer> content1 = new HashMap<String, Integer>();
        content1.put("Nasi Putih",1);
        content1.put("Ayam Bakar",1);
        content1.put("Teh Manis",1);
        Packet packet1 = new Packet(3, "Paket Ayam Bakar",
                32000, content1, 200);
        assertTrue(packet.equals(packet1));

    }

    @Test
    void getContent() {
        Map<String, Integer> content1 = new HashMap<String, Integer>();
        content1.put("Nasi Putih",1);
        content1.put("Ayam Bakar",1);
        content1.put("Teh Manis",1);
        assertEquals(content1, packet.getContent());
    }

    @Test
    void setContent() {
        Map<String, Integer> content1 = new HashMap<String, Integer>();
        content1.put("Nasi Putih",1);
        content1.put("Bebek Bakar",1);
        content1.put("Teh Manis",1);
        packet.setContent(content1);
        assertEquals(content1, packet.getContent());
    }

    @Test
    void getStock() {
        assertEquals(200, packet.getStock());
    }

    @Test
    void setStock() {
        packet.setStock(500);
        assertEquals(500, packet.getStock());
    }
}