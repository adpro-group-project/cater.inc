package com.adprog4.cater_inc.menu.service;

import com.adprog4.cater_inc.menu.model.*;
import com.adprog4.cater_inc.menu.repository.MenuRepository;
import com.adprog4.cater_inc.menu.repository.ProductRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.InvalidObjectException;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MenuServiceimplTest {

    @InjectMocks
    private MenuServiceimpl menuService;

    @Mock
    private MenuRepository menuRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private Random idGenerator;

    @AfterEach
    void tearDown() {
        reset(menuRepository);
        reset(productRepository);
        reset(idGenerator);
    }

    @Test
    void getAllProducts(){
        List<Product> listProduct = new ArrayList<>();
        listProduct.add(new CustomPacket(1));
        listProduct.add(new CustomPacket(2));

        when(productRepository.findAllProductNoTopping()).thenReturn(listProduct);

        assertEquals(listProduct, menuService.getAllProducts());

        verify(productRepository, times(1)).findAllProductNoTopping();
    }

    @Test
    void findProductByName() {
        Optional<Product> optionalProduct = Optional.empty();

        when(productRepository.findProductNoToppingByName("KJKJ")).thenReturn(optionalProduct);

        assertEquals(optionalProduct, menuService.findProductByName("KJKJ"));

        verify(productRepository, times(1)).findProductNoToppingByName("KJKJ");


    }

    @Test
    void getAllToppings() {
        List<Topping> toppingList = new ArrayList<>();

        when(productRepository.findAllTopping()).thenReturn(toppingList);

        assertEquals(toppingList, menuService.getAllToppings());

        verify(productRepository, times(1)).findAllTopping();
    }

    @Test
    void findToppingByName() {
        Optional<Topping> optionalTopping = Optional.empty();

        when(productRepository.findToppingByName("KJKJ")).thenReturn(optionalTopping);

        assertEquals(optionalTopping, menuService.findToppingByName("KJKJ"));

        verify(productRepository, times(1)).findToppingByName("KJKJ");

    }

    @Test
    void getAllMenu() {
        List<Menu> menuList = new ArrayList<Menu>();

        when(menuRepository.findAll()).thenReturn(menuList);

        assertEquals(menuService.getAllMenu(), menuList);

        verify(menuRepository, times(1)).findAll();
    }

    @Test
    void addProduct() {
        ProductDTO productDTO = new ProductDTO("CustomPacket", 1
                , "Custom", BigDecimal.ZERO, null, 1);

        when(productRepository.findById(Long.valueOf(1))).
                thenReturn(Optional.of(new CustomPacket(1)));

        when(productRepository.findById(Long.valueOf(2)))
                .thenReturn(Optional.of(new CustomPacket(1)));

        when(productRepository.findById(Long.valueOf(3)))
                .thenReturn(Optional.empty());

        when(idGenerator.nextInt()).thenReturn(2).thenReturn(3);

        menuService.addProduct(productDTO);

        verify(productRepository, times(1)).save(productDTO.convertToProduct());
    }


    @Test
    void addTopping() {
        ToppingDTO toppingDTO = new ToppingDTO("Food", 1
                , "Custom", BigDecimal.ONE, 1);

        when(productRepository.findById(Long.valueOf(1)))
                .thenReturn(Optional.of(new Food()));

        when(productRepository.findById(Long.valueOf(2)))
                .thenReturn(Optional.of(new Food()));

        when(productRepository.findById(Long.valueOf(3)))
                .thenReturn(Optional.empty());

        when(idGenerator.nextInt()).thenReturn(2).thenReturn(3);

        menuService.addTopping(toppingDTO);

        verify(productRepository, times(1))
                .save(toppingDTO.convertToTopping());
    }

    @Test
    void addMenu() throws InvalidObjectException {
        Product product = new CustomPacket(2);

        Food food = new Food(1, "Ayam Bakar", 12000, 1);
        food.setProduct(product);

        Map<String, Integer> content = new HashMap<>();
        content.put("Ayam Bakar",1);

        MenuDTO menuDTO = new MenuDTO(1, BigDecimal.valueOf(12000.0)
                ,food, product, content);

        when(idGenerator.nextInt()).thenReturn(2).thenReturn(3);

        when(productRepository.findToppingByName("Ayam Bakar"))
                .thenReturn(Optional.of(new Food(food)));

        when(productRepository.findProductNoToppingByName("Custom"))
                .thenReturn(Optional.of(product));

        when(menuRepository.findById(Long.valueOf(1)))
                .thenReturn(Optional.empty());

        menuService.addMenu(menuDTO);

        verify(menuRepository, times(1)).save(Mockito.any(Menu.class));

        verify(productRepository, times(1))
                .findToppingByName("Ayam Bakar");

        verify(productRepository, times(1))
                .findProductNoToppingByName("Custom");
    }

    @Test
    void menuSetBase() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Packet packet = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);

        Menu menu = new Menu(2, new CustomPacket(2));

        MenuProductWrapper menuProductWrapper = new MenuProductWrapper(menu, packet);

        Menu newMenu = menuService.menuSetBase(menuProductWrapper);

        assertTrue(newMenu.getBase().equals(packet));
    }

    @Test
    void menuAddTopping() {
        Food food = new Food(1, "Ayam Kukus",20000, 20);

        Menu menu = new Menu(2, new CustomPacket(2));

        MenuToppingWrapper menuToppingWrapper = new MenuToppingWrapper(menu, food);

        Menu newMenu = menuService.menuAddTopping(menuToppingWrapper);

        assertTrue(newMenu.getProduct().equals(food));
    }

    @Test
    void menuRemoveTopping() {
        Food food = new Food(1, "Ayam Kukus",20000, 1);

        Menu menu = new Menu(2, new CustomPacket(2));

        MenuToppingWrapper menuToppingWrapper = new MenuToppingWrapper(menu, food);

        Menu newMenu = menuService.menuAddTopping(menuToppingWrapper);

        assertTrue(newMenu.getProduct().equals(food));

        menuToppingWrapper.setMenu(newMenu);

        newMenu = menuService.menuRemoveTopping(menuToppingWrapper);

        assertTrue(newMenu.getProduct().equals(new CustomPacket(2)));
    }


    @Test
    void checkMenuPrice() {
        Map<String, Integer> content = new HashMap<String, Integer>();
        content.put("Nasi Putih",1);
        content.put("Ayam Bakar",1);
        content.put("Teh Manis",1);
        Product base = new Packet(2, "Paket Ayam Bakar",
                32000, content, 200);
        MenuDTO menuDTO = new MenuDTO(2, base.getPrice(), base,
                base, new HashMap<String, Integer>());

        assertEquals(BigDecimal.valueOf(32000.0), menuService.checkMenuPrice(menuDTO));
    }

}