package com.adprog4.cater_inc.inbox.model;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.adprog4.cater_inc.user.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class InboxTest {

    private Inbox inbox;

    @Mock
    Message message1 = new Message("1", "Message 1");
    Message message2 = new Message("2", "Message 2");

    @BeforeEach
    public void setUp() {
        inbox = new Inbox();
        inbox.setId(1);
    }

    @Test
    public void testWhenGetId() {
        assertEquals(1, inbox.getId());
    }

    @Test
    public void testWhenSetId() {
        inbox.setId(2);

        assertEquals(2, inbox.getId());
    }

    @Test
    public void testWhenGetCustomer() {
        assertNull(inbox.getCustomer());
    }

    @Test
    public void testWhenSetCustomer() {
        Customer customer = new Customer("1", "test", "test", "test",
                LocalDate.now());
        inbox.setCustomer(customer);
        assertNotNull(inbox.getCustomer());
    }

    @Test
    public void testWhenUpdate() {
        Message message = new Message("Test", "This is just test");
        inbox.update(message);
        assertEquals(1, inbox.getInboxCustomer().size());
    }

    @Test
    public void testWhenSetMessages() {
        List<Message> listOfMessages = new ArrayList<>();
        listOfMessages.add(message1);
        listOfMessages.add(message2);

        inbox.setInboxCustomer(listOfMessages);

        assertEquals(2, inbox.getInboxCustomer().size());
    }

}
