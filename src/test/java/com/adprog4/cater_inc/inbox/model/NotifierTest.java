package com.adprog4.cater_inc.inbox.model;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.adprog4.cater_inc.user.model.Admin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class NotifierTest {

    private Notifier notifier;

    @Mock
    Admin admin = new Admin("1", "test", "test", notifier);
    Inbox inbox1 = new Inbox();
    Inbox inbox2 = new Inbox();
    Message message1 = new Message("1", "Message 1");
    Message message2 = new Message("2", "Message 2");

    @BeforeEach
    public void setUp() {
        notifier = new Notifier();
        notifier.setId(1);
    }

    @Test
    public void testWhenGetId() {
        assertEquals(1, notifier.getId());
    }

    @Test
    public void testWhenSetId() {
        notifier.setId(2);

        assertEquals(2, notifier.getId());
    }

    @Test
    public void testWhenGetAdmin() {
        assertNull(notifier.getAdmin());
    }

    @Test
    public void testWhenSetAdmin() {
        notifier.setAdmin(admin);

        assertNotNull(notifier.getAdmin());
    }

    @Test
    public void testWhenSetInboxes() {
        List<Inbox> listOfInboxes = new ArrayList<>();
        listOfInboxes.add(inbox1);
        listOfInboxes.add(inbox2);

        notifier.setInboxes(listOfInboxes);

        assertEquals(2, notifier.getInboxes().size());
    }

    @Test
    public void testWhenSetMessages() {
        List<Message> listOfMessages = new ArrayList<>();
        listOfMessages.add(message1);
        listOfMessages.add(message2);

        notifier.setMessages(listOfMessages);

        assertEquals(2, notifier.getMessages().size());
    }

    @Test
    public void testWhenRegisterInbox() {
        Inbox inbox = new Inbox();
        notifier.registerInbox(inbox);

        assertEquals(1, notifier.getInboxes().size());
    }

    @Test
    void testWhenRemoveInbox() {
        Inbox inbox = new Inbox();
        notifier.registerInbox(inbox);
        notifier.removeInbox(inbox);

        assertEquals(0, notifier.getInboxes().size());
    }

    @Test
    public void testWhenNotifyInbox() {
        Inbox inbox = new Inbox();
        Message message = new Message("Test", "This is just test");

        notifier.registerInbox(inbox);
        notifier.addMessage(message);

//        verify(notifier, times(1)).notifyInbox(message);
        assertEquals("Test", inbox.getInboxCustomer().get(0).getTitle());

    }

    @Test
    public void testWhenAddMessage() {
        Message message = new Message("Test", "This is just test");

        notifier.addMessage(message);

        assertEquals(1, notifier.getMessages().size());

//        verify(notifier, times(1)).addMessage(message);
    }

}
