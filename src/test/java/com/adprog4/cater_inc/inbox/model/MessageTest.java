package com.adprog4.cater_inc.inbox.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.time.Month;

public class MessageTest {

    private Message message;

    @Mock
    Message message1 = new Message("Test1", "Message 1");

    @BeforeEach
    public void setUp() {
        message = new Message("Test", "This is just test");
        message.setId(1);
    }

    @Test
    public void testWhenGetId() {
        assertEquals(1, message.getId());
    }

    @Test
    public void testWhenSetId() {
        message.setId(2);

        assertEquals(2, message.getId());
    }

    @Test
    public void testWhenGetDate() {
        assertNotEquals(LocalDateTime.now(), message1.getDateTime());
    }

    @Test
    public void testWhenSetDate() {
        message.setDateTime(LocalDateTime.of(2019, Month.MAY, 15, 20, 00, 00));

        assertEquals(LocalDateTime.of(2019, Month.MAY, 15, 20, 00, 00),
                message.getDateTime());
    }

    @Test
    public void testWhenGetTitle() {
        assertEquals("Test", message.getTitle());
    }

    @Test
    public void testWhenSetTitle() {
        message.setTitle("Tset");

        assertEquals("Tset", message.getTitle());
    }
    @Test
    public void testWhenGetBody() {
        assertEquals("This is just test", message.getBody());
    }

    @Test
    public void testWhenSetBody() {
        message.setBody("Blank");

        assertEquals("Blank", message.getBody());
    }
}
