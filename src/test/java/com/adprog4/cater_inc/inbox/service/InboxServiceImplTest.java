package com.adprog4.cater_inc.inbox.service;

import com.adprog4.cater_inc.inbox.model.Inbox;
import com.adprog4.cater_inc.inbox.model.Message;
import com.adprog4.cater_inc.inbox.model.Notifier;
import com.adprog4.cater_inc.inbox.repository.InboxRepository;
import com.adprog4.cater_inc.inbox.repository.MessageRepository;
import com.adprog4.cater_inc.inbox.repository.NotifierRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class InboxServiceImplTest {

    @InjectMocks
    private InboxServiceImpl inboxService;

    @Mock
    private InboxRepository inboxRepository;

    @Mock
    private MessageRepository messageRepository;

    @Mock
    private NotifierRepository notifierRepository;

    @AfterEach
    void tearDown() {
        reset(inboxRepository);
        reset(messageRepository);
        reset(notifierRepository);
    }

    @Test
    void testGetNotifierById() {
        Optional<Notifier> optionalNotifier = Optional.empty();

        when(notifierRepository.findById(Long.valueOf(1))).thenReturn(optionalNotifier);

        assertEquals(optionalNotifier, inboxService.getNotifierById(Long.valueOf(1)));

        verify(notifierRepository, times(1)).findById(Long.valueOf(1));
    }

    @Test
    void testCreateNotifier() {
        Notifier notifier = new Notifier();
        notifier.setId(1);

        inboxService.createNotifier(notifier);
        lenient().when(inboxService.createNotifier(notifier)).thenReturn(notifier);
        assertEquals(notifier, inboxService.createNotifier(notifier));
    }

    @Test
    void testUpdateNotifier() {
        Notifier notifier = new Notifier();
        notifier.setId(1);

        inboxService.createNotifier(notifier);
        assertEquals(notifier, inboxService.updateNotifier(notifier));
    }

    @Test
    void testDeleteNotifier() {
        Notifier notifier = new Notifier();
        notifier.setId(1);

        inboxService.createNotifier(notifier);
        inboxService.deleteNotifier(Long.valueOf(1));
        lenient().when(inboxService.getNotifierById(notifier.getId())).thenReturn(Optional.empty());
    }

    @Test
    void testAddInboxToNotifier() {
        Notifier notifier = new Notifier();
        notifier.setId(1);

        Inbox inbox = new Inbox();
        inbox.setId(1);

        when(notifierRepository.findById(Long.valueOf(1))).thenReturn(Optional.of(notifier));
        when(inboxRepository.findById(Long.valueOf(1))).thenReturn(Optional.of(inbox));
        inboxService.addInboxToNotifier(Long.valueOf(1), Long.valueOf(1));

        assertEquals(1, notifier.getInboxes().size());
    }

    @Test
    void testAddMessageToNotifier() {
        Notifier notifier = new Notifier();
        notifier.setId(1);

        Message message = new Message("test", "test");
        message.setId(2);

        when(notifierRepository.findById(Long.valueOf(1))).thenReturn(Optional.of(notifier));
        when(messageRepository.findById(Long.valueOf(1))).thenReturn(Optional.of(message));
        inboxService.addMessageToNotifier(Long.valueOf(1), Long.valueOf(1));

        assertEquals(1, notifier.getMessages().size());
    }

    @Test
    void testGetAllInboxes() {
        List<Inbox> listInbox = new ArrayList<>();
        Inbox testInboxObject1 = new Inbox();
        testInboxObject1.setId(1);
        Inbox testInboxObject2 = new Inbox();
        testInboxObject2.setId(2);
        listInbox.add(testInboxObject1);
        listInbox.add(testInboxObject2);

        when(inboxRepository.findAll()).thenReturn(listInbox);

        assertEquals(listInbox, inboxService.getAllInboxes());

        verify(inboxRepository, times(1)).findAll();
    }

    @Test
    void testGetInboxById() {
        Optional<Inbox> optionalInbox = Optional.empty();

        when(inboxRepository.findById(Long.valueOf(1))).thenReturn(optionalInbox);

        assertEquals(optionalInbox, inboxService.getInboxById(Long.valueOf(1)));

        verify(inboxRepository, times(1)).findById(Long.valueOf(1));
    }

    @Test
    void testGetAllMessagesInInbox() {
        Inbox inbox = new Inbox();
        inbox.setId(1);

        when(inboxRepository.findById(Long.valueOf(1))).thenReturn(Optional.of(inbox));


        assertEquals(0, inboxService.getAllMessagesInInbox(Long.valueOf(1)).size());
    }

    @Test
    void testCreateInbox() {
        Inbox inbox = new Inbox();
        inbox.setId(1);

        inboxService.createInbox(inbox);
        lenient().when(inboxService.createInbox(inbox)).thenReturn(inbox);
        assertEquals(inbox, inboxService.createInbox(inbox));
    }

    @Test
    void testUpdateInbox() {
        Inbox inbox = new Inbox();
        inbox.setId(1);

        inboxService.createInbox(inbox);
        assertEquals(inbox, inboxService.updateInbox(inbox));
    }

    @Test
    void testDeleteInbox() {
        Inbox inbox = new Inbox();
        inbox.setId(1);

        inboxService.createInbox(inbox);
        inboxService.deleteInbox(Long.valueOf(1));
        lenient().when(inboxService.getInboxById(inbox.getId())).thenReturn(Optional.empty());
    }

    @Test
    void testGetMessages() {
        List<Message> listMessage = new ArrayList<>();
        Message testMessageObject1 = new Message("test", "test");
        testMessageObject1.setId(1);
        Message testMessageObject2 = new Message("test", "test");
        testMessageObject2.setId(2);
        listMessage.add(testMessageObject1);
        listMessage.add(testMessageObject2);

        when(messageRepository.findAll()).thenReturn(listMessage);

        assertEquals(listMessage, inboxService.getAllMessages());

        verify(messageRepository, times(1)).findAll();
    }

    @Test
    void testGetMessageById() {
        Optional<Message> optionalMessage = Optional.empty();

        when(messageRepository.findById(Long.valueOf(1))).thenReturn(optionalMessage);

        assertEquals(optionalMessage, inboxService.getMessageById(Long.valueOf(1)));

        verify(messageRepository, times(1)).findById(Long.valueOf(1));
    }

    @Test
    void testCreateMessage() {
        Message message = new Message("test", "test");
        message.setId(1);

        inboxService.createMessage(message);
        lenient().when(inboxService.createMessage(message)).thenReturn(message);
        assertEquals(message, inboxService.createMessage(message));
    }

    @Test
    void testUpdateMessage() {
        Message message = new Message("test", "test");
        message.setId(1);

        inboxService.createMessage(message);
        assertEquals(message, inboxService.updateMessage(message));
    }

    @Test
    void testDeleteMessage() {
        Message message = new Message("test", "test");
        message.setId(1);

        inboxService.createMessage(message);
        inboxService.deleteMessage(Long.valueOf(1));
        lenient().when(inboxService.getMessageById(message.getId())).thenReturn(Optional.empty());
    }
}
