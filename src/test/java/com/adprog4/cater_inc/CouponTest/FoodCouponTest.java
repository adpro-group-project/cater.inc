package com.adprog4.cater_inc.CouponTest;
import com.adprog4.cater_inc.menu.model.*;

import com.adprog4.cater_inc.pemesanan.factory.model.FoodCoupon;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FoodCouponTest {
    @Mock
    private FoodCoupon foodCoupon;
    private Menu menu;
    private Product base;
    private Pemesanan pemesanan;


    @BeforeEach
    public void setUp() {
        base = new Packet(2, "Paket Ayam Bakar",
                BigDecimal.valueOf(110000), null, 200);
        menu = new Menu(0,base);
        foodCoupon = new FoodCoupon(BigDecimal.valueOf(0.1), "Just Test");
        pemesanan = new Pemesanan(menu);
        this.pemesanan.setCoupon(foodCoupon);
    }

    @Test
    public void testGetDescription() {
        assertEquals("Just Test", foodCoupon.getDescription());
    }

    @Test
    public void testGetMenuPrice() {
        assertEquals(BigDecimal.valueOf(109000.0), foodCoupon.getMenuPrice(pemesanan));
    }

}
