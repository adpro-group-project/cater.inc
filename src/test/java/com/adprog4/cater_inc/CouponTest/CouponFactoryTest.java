package com.adprog4.cater_inc.CouponTest;

import com.adprog4.cater_inc.pemesanan.factory.model.CouponFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CouponFactoryTest {
    private CouponFactory couponFactory;

    @BeforeEach
    public void setUp() {
        couponFactory = new CouponFactory();
    }

    @Test
    public void testGetCouponFactory() {
        assertEquals("FoodCoupon", couponFactory.getCouponPrice("Discount", BigDecimal.valueOf(20000.0), "Just Test").getClass().getSimpleName());
    }

}


