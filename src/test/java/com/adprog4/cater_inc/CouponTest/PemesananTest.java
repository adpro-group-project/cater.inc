package com.adprog4.cater_inc.CouponTest;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.pemesanan.factory.model.Coupon;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import com.adprog4.cater_inc.pemesanan.factory.model.ShipmentCoupon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PemesananTest {
    private Pemesanan pemesanan;
    private Menu menu;
    private Packet base;

    @Mock
    Coupon shipmentCoupon = new ShipmentCoupon(BigDecimal.valueOf(10000), "Just Test");


    @BeforeEach
    public void setUp(){
        base = new Packet(2, "Paket Ayam Bakar",BigDecimal.valueOf(110000), null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(menu);
        pemesanan.setCoupon(shipmentCoupon);
    }

    @Test
    public void testSetFoodPrice() {
        pemesanan.setFoodPrice(BigDecimal.valueOf(100000));
        assertEquals(BigDecimal.valueOf(100000), pemesanan.getFoodPrice());
    }

    @Test
    public void testGetFoodPrice() {
        assertEquals(BigDecimal.valueOf(110000), pemesanan.getFoodPrice());
    }

    @Test
    public void testSetShipmentPrice() {
        pemesanan.setShipmentPrice(BigDecimal.valueOf(5000));
        assertEquals(BigDecimal.valueOf(5000), pemesanan.getShipmentPrice());
    }

    @Test
    public void testGetShipmentPrice() {
        assertEquals(BigDecimal.valueOf(10000), pemesanan.getShipmentPrice());
    }

    @Test
    public void testGetPricePemesanan() {
        assertEquals(BigDecimal.valueOf(110000), shipmentCoupon.getMenuPrice(pemesanan));
    }

    @Test
    public void testGetDiscountPrice() {
        assertEquals(BigDecimal.valueOf(10000), shipmentCoupon.getDiscountPrice(pemesanan));
    }
}
