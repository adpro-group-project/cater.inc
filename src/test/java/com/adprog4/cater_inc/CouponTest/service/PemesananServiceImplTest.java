package com.adprog4.cater_inc.CouponTest.service;


import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import com.adprog4.cater_inc.pemesanan.factory.repository.PemesananRepository;
import com.adprog4.cater_inc.pemesanan.factory.service.PemesananServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@ExtendWith(MockitoExtension.class)
class PemesananServiceImplTest {

    @InjectMocks
    private PemesananServiceImpl pemesananService;

    @Mock
    private PemesananRepository pemesananRepository;

    @Mock
    private Menu menu1;
    private Menu menu2;
    private Pemesanan pemesanan1;
    private Pemesanan pemesanan2;



    @BeforeEach
    void setUp(){
        Packet base1 = new Packet(2, "Paket Ayam Bakar",BigDecimal.valueOf(110000), null, 200);
        Packet base2 = new Packet(3, "Paket Ayam Rebus",BigDecimal.valueOf(100000), null, 200);
        menu1 = new Menu(0,base1);
        menu2 = new Menu(1, base2);
        pemesanan1 = new Pemesanan(menu1);
        pemesanan2 = new Pemesanan(menu2);
        pemesananRepository.save(pemesanan1);
    }

    @AfterEach
    void tearDown(){
        reset(pemesananRepository);
    }

    @Test
    void newPemesanan(){
        int jumlahPemesanan = pemesananRepository.findAll().size();
        pemesananService.newPemesanan(menu2);
        assertEquals(0, pemesananRepository.findAll().size());
    }
}

