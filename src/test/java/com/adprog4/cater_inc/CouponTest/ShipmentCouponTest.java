package com.adprog4.cater_inc.CouponTest;

import com.adprog4.cater_inc.menu.model.Menu;
import com.adprog4.cater_inc.menu.model.Packet;
import com.adprog4.cater_inc.menu.model.Product;
import com.adprog4.cater_inc.pemesanan.factory.model.Pemesanan;
import com.adprog4.cater_inc.pemesanan.factory.model.ShipmentCoupon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipmentCouponTest {
    @Mock
    private ShipmentCoupon shipmentCoupon;
    private Pemesanan pemesanan;
    private Product base;
    private Menu menu;

    @BeforeEach
    public void setUp() {
        shipmentCoupon = new ShipmentCoupon(BigDecimal.valueOf(10000), "Just Test");
        base = new Packet(2, "Paket Ayam Bakar",
                BigDecimal.valueOf(110000), null, 200);
        menu = new Menu(0,base);
        pemesanan = new Pemesanan(menu);
        pemesanan.setCoupon(shipmentCoupon);

    }

    @Test
    public void testGetDescription() {
        assertEquals("Just Test", shipmentCoupon.getDescription());
    }

    @Test
    public void testGetMenuPrice() {
        assertEquals(BigDecimal.valueOf(110000), shipmentCoupon.getMenuPrice(pemesanan));
    }
}
