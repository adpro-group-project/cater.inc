package com.adprog4.cater_inc.user;

import com.adprog4.cater_inc.user.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

public class CustomerTest {
    Customer customer;

    @BeforeEach
    void setUp() {
        customer = new Customer("123", "Jilham", "Luthfi", "test",
                LocalDate.now());
    }

    @Test
    void testGetUuid() {
        assertEquals("123", customer.getUuid());
    }

    @Test
    void testSetUuid() {
        customer.setUuid("321");
        assertEquals("321", customer.getUuid());
    }

    @Test
    void testGetFirstName() {
        assertEquals("Jilham", customer.getFirstName());
    }

    @Test
    void testSetFirstName() {
        customer.setFirstName("Jilly");
        assertEquals("Jilly", customer.getFirstName());
    }

    @Test
    void testGetLastName() {
        assertEquals("Luthfi", customer.getLastName());
    }

    @Test
    void testSetLastName() {
        customer.setLastName("Upi");
        assertEquals("Upi", customer.getLastName());
    }

    @Test
    void testGetAddress() {
        assertEquals("test", customer.getAddress());
    }

    @Test
    void testSetAddress() {
        customer.setAddress("fasilkom");
        assertEquals("fasilkom", customer.getAddress());
    }

    @Test
    void testGetBirthDate() {
        assertEquals(LocalDate.now(), customer.getBirthDate());
    }

    @Test
    void testSetBirthDate() {
        customer.setBirthDate(LocalDate.of(2000, 5, 15));
        assertEquals(LocalDate.of(2000, 5, 15), customer.getBirthDate());
    }
}
