package com.adprog4.cater_inc.user;

import com.adprog4.cater_inc.inbox.model.Notifier;
import com.adprog4.cater_inc.user.model.Admin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class AdminTest {
    Admin admin;

    @Mock
    Notifier notifier = new Notifier();

    @BeforeEach
    void setUp() {
        admin = new Admin("100", "Jilham", "Luthfi", notifier);
    }

    @Test
    void testGetNotifier() {
        assertEquals(notifier, admin.getNotifier());
    }
}
