## Cater.Inc


## Authors

1. [Ferenica Dwi Putri](https://gitlab.com/ferenica)
2. [Muhammad Jilham Luthfi](https://gitlab.com/JillyCS18)
3. [Muhammad Rasyid](https://gitlab.com/mrasyid22)
4. [Rafif Elfazri](https://gitlab.com/RafifEL)

## Features

- Menu: the customer can view menus provided by Cater.inc, i.e., packet menu and custom menu

- Inbox: the customer is notified by the system when there is an important message, such as order, promo, or new menu, through this feature

- Wishlist: the customer can save their favorite menu

- Status Pemesanan: the customer can find out the status of the order

- Pemesanan: the customer can order the menu from Cater.Inc

- Promo: the customer can choose two types of coupon, discount voucher or shipment voucher


## Feature Handler

- Feren     : Promo, Pemesanan
- Jilham    : Inbox, Wishlist
- Rasyid    : Status Pemesanan, Pemesanan
- Rafif     : Menu

